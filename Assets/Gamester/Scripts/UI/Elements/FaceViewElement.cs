﻿using System.Collections;
using System.Collections.Generic;
using Gamester.Backend;
using GamesterTools;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class FaceViewElement : MonoBehaviour
    {
        [SerializeField] GameFace gameFace;
        [SerializeField] Text text;
        [SerializeField] Button button;

        [Inject] MainMenuState mainMenuState;
        [Inject] MenuTransitionerController transitioner;

        public void SetFaceId(string faceId)
        {
            gameFace.ChangeFace(faceId);
            text.text = faceId;

            button.onClick.AddListener(() =>
            {
                mainMenuState.selectedFaceId.Value = faceId;
                transitioner.NextMenuId = "CustomizeCharacterMenu";
                transitioner.Show("MakeFaceMenu", TransitionDirection.forward);
            });
        }
    }
}