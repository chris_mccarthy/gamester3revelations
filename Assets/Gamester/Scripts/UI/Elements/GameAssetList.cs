﻿using Gamester.Backend;
using Gamester.Data;
using GFS.BackendTools;
using GFS.Utilities;
using UnityEngine;
using Zenject;

namespace Gamester.UI
{
    public class GameAssetList : AssetListBase<GameData>
    {
        [SerializeField]
        bool showOwn;

        [Inject]
        private MainMenuState mainMenuState;
        [Inject]
        private ClientState clientState;
        [Inject]
        private MenuTransitionerController transitioner;
        [Inject]
        private IRepository<CharacterData> characterData;
        [Inject]
        private IAlertStrategy alertStrategy;

        protected override void Start()
        {
            isStatic = !showOwn;
            base.Start();
        }

        protected override void ViewElementClicked(string id)
        {
            mainMenuState.selectedGameId.Value = id;
            transitioner.Show("PreviewGameMenu", TransitionDirection.forward);
        }

        protected override void AddAssetClicked()
        {
            if (characterData.HasKeys(clientState.userId.Value))
                transitioner.Show("SelectGenreMenu", TransitionDirection.forward);
            else
            {
                mainMenuState.selectedFaceId.Value = "regular";
                transitioner.NextMenuId = "SelectGenreMenu";
                transitioner.Show("MakeFaceMenu", TransitionDirection.forward);
            }
        }

        protected override bool FilterAsset(GameData asset)
        {
            bool isOwn = asset.ownerUserId == clientState.userId.Value;
            return (isOwn && showOwn) || (!isOwn && !showOwn);
        }

        protected override void HoldAction(string key)
        {
            alertStrategy.ShowAlert(new AlertParameters
            {
                text = "Delete?",
                subtext = "Are you sure you want to delete this?",
                buttons = new[]{
                    new AlertButton{
                        text = "Yes",
                        action = () => repo.DeleteKey(key)
                    },
                    new AlertButton{
                        text = "No"
                    }
                }
            });
        }
    }
}