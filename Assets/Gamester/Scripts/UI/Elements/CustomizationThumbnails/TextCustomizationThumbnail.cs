﻿using Gamester.Game;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class TextCustomizationThumbnail : MonoBehaviour
    {
        [SerializeField]
        private Text text;

        [Inject]
        GameCustomization customization;
        [Inject]
        DiContainer container;

        private void Start()
        {
            text.text = customization.id;
        }
    }
}