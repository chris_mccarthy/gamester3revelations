﻿using System.Linq;
using Gamester.Game;
using GamesterTools;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class TextureCustomizationThumbnail : MonoBehaviour
    {
        [SerializeField]
        private TextureDisplays textureDisplays;
        [SerializeField]
        private string key;

        [Inject]
        GameCustomization customization;
        [Inject]
        DiContainer container;

        private void Start()
        {
            var assetCollection = customization.asset as CustomizationAssetCollection;
            var assetKVP = assetCollection.assets.FirstOrDefault(kvp => kvp.key == key);
            var texture = assetKVP.asset as Texture2D;
            texture.ApplyToDisplay(textureDisplays);
        }
    }
}