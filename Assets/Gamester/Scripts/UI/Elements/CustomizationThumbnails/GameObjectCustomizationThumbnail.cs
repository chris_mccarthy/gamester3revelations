﻿using Gamester.Game;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class GameObjectCustomizationThumbnail : MonoBehaviour
    {
        [SerializeField]
        private Transform thumbnailParent;
        [SerializeField]
        private float thumbnailScale;

        [Inject]
        GameCustomization customization;
        [Inject]
        DiContainer container;

        private void Start()
        {
            var instance = container.InstantiatePrefab(customization.asset as GameObject, thumbnailParent);
            instance.transform.localPosition = Vector3.zero;
            instance.transform.localScale = Vector3.one * thumbnailScale;
            instance.transform.localEulerAngles = Vector3.zero;
        }
    }
}