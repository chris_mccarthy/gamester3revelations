﻿using GFS.BackendTools;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class FBLoginButton : MonoBehaviour
    {
        [SerializeField]
        private Button fbLoginButton;

        [Inject]
        private ClientState clientState;
        [Inject]
        private AuthenticationController authenticationController;

        private void Start()
        {
            clientState.isLocked
                .TakeUntilDestroy(this)
                .Subscribe(isLocked => fbLoginButton.interactable = !isLocked);
            clientState.isFBLinked
                .TakeUntilDestroy(this)
                .Subscribe(isFBLinked => fbLoginButton.gameObject.SetActive(!isFBLinked));

            fbLoginButton.onClick.AddListener(authenticationController.LoginToFacebook);
        }
    }
}