﻿using System.Linq;
using Gamester.Data;
using Gamester.Game;
using GFS.BackendTools;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class GameViewElement : MonoBehaviour
    {
        [SerializeField]
        private Transform root;

        [Inject]
        private GameData gameData;
        [Inject]
        private GenreCollection genreCollection;
        [Inject]
        private IRepository<CharacterData> characterRepo;
        [Inject]
        private DiContainer container;

        private void Start()
        {
            var subcontainer = container.CreateSubContainer();
            subcontainer.BindInstance(characterRepo.GetData(gameData.characterId).Value);
            subcontainer.BindInstance(gameData);

            var previewPrefab = genreCollection.genres.First(genre => genre.genreID == gameData.genreId).previewPrefab;
            var previewInstance = subcontainer.InstantiatePrefab(previewPrefab, root);
            previewInstance.transform.SetAsFirstSibling();
        }
    }
}