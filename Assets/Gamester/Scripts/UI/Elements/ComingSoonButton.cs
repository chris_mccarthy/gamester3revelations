﻿using GFS.Utilities;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    [RequireComponent(typeof(Button))]
    public class ComingSoonButton : MonoBehaviour
    {
        [Inject]
        IAlertStrategy alertController;

        void Start()
        {
            var button = GetComponent<Button>();
            button.onClick.AddListener(() =>
            {
                alertController.ShowAlert(new AlertParameters
                {
                    text = "Coming Soon!",
                    subtext = "The feature you selected is not availible",
                    buttons = new[] {
                        new AlertButton {
                            text = "Okay"
                        }
                    }
                });
            });
        }
    }
}