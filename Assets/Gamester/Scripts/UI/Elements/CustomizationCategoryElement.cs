﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gamester.Game;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using MagneticScrollView;

namespace Gamester.UI
{
    public class CustomizationCategoryElement : MonoBehaviour
    {
        [SerializeField]
        Transform thumbnailParents;
        [SerializeField]
        Text categoryText;
        [SerializeField]
        MagneticScrollRect scrollView;

        [Inject]
        GenreCollection genreAssets;
        [Inject]
        DiContainer container;

        public IObservable<string> Init(string genreId, string categoryId, string selectedCustomizationId)
        {
            Subject<string> returned = new Subject<string>();

            categoryText.text = categoryId;

            var customizations = genreAssets.genres.First(genre => genre.genreID == genreId).customizations.Where(customization => customization.categoryId == categoryId);
            var children = new List<Tuple<GameObject, string>>();
            int counter = 0;
            int scrollTo = 0;
            foreach (var customization in customizations)
            {
                if (customization.id == selectedCustomizationId)
                    scrollTo = counter;

                var subcontainer = container.CreateSubContainer();
                subcontainer.BindInstance(customization);

                var instantiated = subcontainer.InstantiatePrefab(customization.thumbnail);
                instantiated.transform.parent = thumbnailParents;
                instantiated.transform.localScale = Vector3.one;

                children.Add(new Tuple<GameObject, string>(instantiated, customization.id));
                counter++;
            }

            Observable.EveryEndOfFrame()
                .First()
                .Subscribe(_ =>
                {
                    scrollView.InstantScrollTo(scrollTo);

                    scrollView.onSelectionChange.AddListener(selected =>
                    {
                        if (selected != null)
                            returned.OnNext(children.First(tuple => tuple.Item1 == selected).Item2);
                    });
                });

            return returned;
        }
    }
}