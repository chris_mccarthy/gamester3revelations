﻿using System.Linq;
using Gamester.Backend;
using Gamester.Data;
using Gamester.Game;
using GFS.BackendTools;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class CustomizeGameMenu : MonoBehaviour
    {
        [SerializeField]
        CustomizationCategoryElement categoryElementPrefab;
        [SerializeField]
        Transform elementsParent;
        [SerializeField]
        Button SaveButton;
        [SerializeField]
        Button CancelButton;
        [SerializeField]
        Transform previewParent;

        [Inject]
        MainMenuState mainMenuState;
        [Inject]
        IRepository<GameData> gameRepository;
        [Inject]
        IRepository<CharacterData> characterRepository;
        [Inject]
        GenreCollection genreCollection;
        [Inject]
        DiContainer container;
        [Inject]
        MenuTransitionerController transitionerController;

        void Start()
        {
            Subject<Unit> refreshPreviewSubject = new Subject<Unit>();

            var gameData = new GameData(gameRepository.GetData(mainMenuState.selectedGameId.Value).Value);

            var genreId = gameData.genreId;
            var genreAssets = genreCollection.genres.First(genre => genre.genreID == genreId);
            var categories = genreAssets.customizations.Select(customization => customization.categoryId).Distinct();

            foreach (var categoryId in categories)
            {
                var instance = container.InstantiatePrefab(categoryElementPrefab);
                instance.transform.parent = elementsParent;
                instance.transform.position = Vector3.zero;
                instance.transform.localScale = Vector3.one;

                var selectedCustomizationId = gameData.customizations.FirstOrDefault(customization => customization.category == categoryId).id;
                if (selectedCustomizationId == null)
                    selectedCustomizationId = gameData.customizations.First().category;
                var selectionStream = instance.GetComponent<CustomizationCategoryElement>().Init(genreId, categoryId, selectedCustomizationId);

                var usedCategoryId = categoryId;
                selectionStream
                    .TakeUntilDestroy(this)
                    .Subscribe(customizationId =>
                    {
                        var removed = gameData.customizations.First(customization => customization.category == usedCategoryId);
                        gameData.customizations.Remove(removed);
                        gameData.customizations.Add(new CustomizationData() { category = usedCategoryId, id = customizationId });

                        refreshPreviewSubject.OnNext(Unit.Default);
                    });
            }

            SaveButton.onClick.AddListener(() =>
            {
                gameRepository.SetData(gameData.id, gameData);

                transitionerController.Show("PreviewGameMenu", TransitionDirection.backwards);
            });

            CancelButton.onClick.AddListener(() =>
            {
                transitionerController.Show("PreviewGameMenu", TransitionDirection.backwards);
            });

            GameObject currentPreview = null;
            var previewPrefab = genreCollection.genres.First(genre => genre.genreID == gameData.genreId).previewPrefab;
            var previewSubcontainer = container.CreateSubContainer();
            previewSubcontainer.BindInstance(gameData);
            previewSubcontainer.BindInstance(characterRepository.GetData(gameData.characterId).Value);

            refreshPreviewSubject.TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    if (currentPreview != null)
                        Destroy(currentPreview);

                    currentPreview = previewSubcontainer.InstantiatePrefab(previewPrefab);
                    currentPreview.transform.SetParent(previewParent);
                    currentPreview.transform.localPosition = Vector3.zero;
                    currentPreview.transform.localScale = Vector3.one;
                });

            refreshPreviewSubject.OnNext(Unit.Default);
        }
    }
}