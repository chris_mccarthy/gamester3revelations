﻿using Gamester.Backend;
using Gamester.Data;
using GFS.BackendTools;
using GFS.Utilities;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class HamburgerMenu : MonoBehaviour
    {
        [SerializeField]
        private Button makeGameButton;
        [SerializeField]
        private Button playGameButton;
        [SerializeField]
        private Button pPButton;

        [Inject] MenuTransitionerController transitioner;
        [Inject] IRepository<CharacterData> characterRepo;
        [Inject] IRepository<GameData> gameRepo;
        [Inject] BuildSettings buildSettings;
        [Inject] ClientState clientState;
        [Inject] MainMenuState mainMenuState;

        private void Start()
        {
            makeGameButton.onClick.AddListener(() =>
            {
                if (characterRepo.HasKeys(clientState.userId.Value))
                    transitioner.ExitHamburger("SelectGenreMenu");
                else
                {
                    mainMenuState.selectedCharacterId.Value = string.Empty;
                    mainMenuState.selectedFaceId.Value = "regular";
                    transitioner.ExitHamburger("MakeFaceMenu");
                }
            });

            playGameButton.onClick.AddListener(() =>
            {
                transitioner.ExitHamburger("PlayGameMenu");
            });

            pPButton.onClick.AddListener(() => Application.OpenURL(buildSettings.privacyPolicyURL));
        }
    }
}