﻿using Gamester.Backend;
using Gamester.Data;
using GFS.BackendTools;
using GFS.Utilities;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField]
        private Button makeGameButton;
        [SerializeField]
        private Button playGameButton;

        [Inject] MenuTransitionerController transitioner;
        [Inject] IRepository<CharacterData> characterRepo;
        [Inject] IRepository<GameData> gameRepo;
        [Inject] ClientState clientState;
        [Inject] MainMenuState mainMenuState;

        private void Start()
        {
            makeGameButton.onClick.AddListener(() =>
            {
                if (characterRepo.HasKeys(clientState.userId.Value))
                    transitioner.Show("SelectGenreMenu", TransitionDirection.forward);
                else
                {
                    mainMenuState.selectedCharacterId.Value = string.Empty;
                    mainMenuState.selectedFaceId.Value = "regular";
                    transitioner.NextMenuId = "SelectGenreMenu";
                    transitioner.Show("MakeFaceMenu", TransitionDirection.forward);
                }
            });
            playGameButton.onClick.AddListener(() =>
            {
                transitioner.Show("PlayGameMenu", TransitionDirection.forward);
            });
        }
    }
}