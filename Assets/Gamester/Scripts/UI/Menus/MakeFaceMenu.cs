﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gamester.Backend;
using Gamester.Data;
using Gamester.Photo;
using GFS.BackendTools;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class MakeFaceMenu : MonoBehaviour
    {
        [System.Serializable]
        private class TakePhotoMenu
        {
            public GameObject root;
            public Button takePhotoButton;
            public Button flipCameraButton;
            public Button usePhotoButton;
            public Button backButton;
        }

        [System.Serializable]
        private class EditPhotoMenu
        {
            public GameObject root;
            public Button backButton;
            public Button doneButton;
            public RawImage photoCanvas;
        }

        [SerializeField]
        private PhotoEditor photoEditor;
        [SerializeField]
        private PhotoTaker photoTaker;
        [SerializeField]
        private CameraRollLoader cameraRollLoader;
        [SerializeField]
        private TakePhotoMenu takePhotoMenu;
        [SerializeField]
        private EditPhotoMenu editPhotoMenu;
        [SerializeField]
        private PhotoSaver.SavePhotoParams savePhotoParams;

        [Inject]
        private MenuTransitionerController menuTransitioner;
        [Inject]
        private PhotoSaver photoSaver;
        [Inject]
        private DiContainer container;
        [Inject]
        private IRepository<CharacterData> characterDataRepository;
        [Inject]
        private IRepository<CharacterFaceAsset> faceAssetRepository;
        [Inject]
        private MainMenuState mainMenuState;
        [Inject]
        private ClientState clientState;

        private bool inPhotoEditMenu;

        void Start()
        {
            editPhotoMenu.root.SetActive(false);
            takePhotoMenu.root.SetActive(true);

            inPhotoEditMenu = false;

            takePhotoMenu.flipCameraButton.onClick.AddListener(photoTaker.FlipCamera);

            takePhotoMenu.takePhotoButton.onClick.AddListener(() =>
            {
                Texture texture = photoTaker.TakePhoto();

                editPhotoMenu.photoCanvas.texture = texture;

                ActivatePhotoEditMenu();
            });

            takePhotoMenu.usePhotoButton.onClick.AddListener(() =>
            {
                photoTaker.ResetCanvasTransforms();
                cameraRollLoader.LoadCameraRollPhoto((bool wasSuccessful) =>
                {
                    if (wasSuccessful)
                        ActivatePhotoEditMenu();
                });
            });

            takePhotoMenu.backButton.gameObject.SetActive(menuTransitioner.CanGoBack);
            takePhotoMenu.backButton.onClick.AddListener(menuTransitioner.Back);

            editPhotoMenu.backButton.onClick.AddListener(() =>
            {
                photoEditor.Reset();
                photoEditor.Deactivate();

                editPhotoMenu.root.SetActive(false);
                takePhotoMenu.root.SetActive(true);

                photoTaker.ActivateCamera();
            });

            editPhotoMenu.doneButton.onClick.AddListener(() =>
            {
                photoSaver.SavePhoto(savePhotoParams, photo =>
                {
                    string characterId;
                    string faceAssetId;
                    string faceId = mainMenuState.selectedFaceId.Value;

                    if (string.IsNullOrEmpty(mainMenuState.selectedCharacterId.Value))
                    {
                        characterId = Guid.NewGuid().ToString();
                        faceAssetId = Guid.NewGuid().ToString();
                    }
                    else
                    {
                        characterId = mainMenuState.selectedCharacterId.Value;
                        var faceData = characterDataRepository.GetData(characterId).Value.faceData.FirstOrDefault(face => face.id == faceId);
                        faceAssetId = (faceData == null) ? Guid.NewGuid().ToString() : faceData.faceAssetId;
                    }

                    CharacterData characterData;

                    if (characterDataRepository.Keys.Value.Contains(characterId))
                    {
                        characterData = characterDataRepository.GetData(characterId).Value;

                        var currentFace = characterData.faceData.FirstOrDefault(faceData => faceData.id == faceId);
                        if (currentFace != null)
                        {
                            characterData.faceData.Remove(currentFace);
                        }
                    }
                    else
                    {
                        characterData = new CharacterData
                        {
                            id = characterId,
                            ownerId = clientState.userId.Value,
                            faceData = new List<CharacterFaceData>()
                        };
                    }

                    characterData.faceData.Add(new CharacterFaceData
                    {
                        id = mainMenuState.selectedFaceId.Value,
                        faceAssetId = faceAssetId
                    });

                    characterDataRepository.SetData(characterId, characterData);
                    faceAssetRepository.SetData(faceAssetId, new CharacterFaceAsset { faceTexture = photo });
                    mainMenuState.selectedCharacterId.Value = characterId;
                    menuTransitioner.Next();
                });
            });
        }

        private void ActivatePhotoEditMenu()
        {
            photoEditor.Activate();

            editPhotoMenu.root.SetActive(true);
            takePhotoMenu.root.SetActive(false);

            inPhotoEditMenu = true;
        }

        private void OnEnable()
        {
            if (inPhotoEditMenu)
            {
                photoEditor.Activate();
            }
        }
    }
}