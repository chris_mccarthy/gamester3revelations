﻿using System.Linq;
using Gamester.Backend;
using Gamester.Data;
using Gamester.Game;
using GFS.BackendTools;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class GamePreviewMenu : MonoBehaviour
    {
        [System.Serializable]
        private class GUIElements
        {
            public Transform previewRoot;
            public Button playButton;
            public Button editButton;
            public Button backButton;
            public InputField titleInputField;
        }

        [SerializeField]
        private GUIElements guiElements;

        [Inject]
        private GenreCollection genreCollection;
        [Inject]
        private MenuTransitionerController menuTransitioner;
        [Inject]
        private DiContainer container;
        [Inject]
        private MainMenuState mainMenuState;
        [Inject]
        private IRepository<CharacterData> characterRepo;
        [Inject]
        private IRepository<GameData> gameRepo;
        [Inject]
        private ClientState clientState;

        private GameObject currentPreview;

        private void Start()
        {
            var selectedGameId = mainMenuState.selectedGameId.Value;
            var selectedGame = gameRepo.GetData(selectedGameId).Value;

            bool isMyGame = selectedGame.ownerUserId == clientState.userId.Value;

            guiElements.editButton.gameObject.SetActive(isMyGame);
            guiElements.titleInputField.interactable = isMyGame;

            guiElements.backButton.gameObject.SetActive(menuTransitioner.CanGoBack);
            guiElements.backButton.onClick.AddListener(menuTransitioner.Back);

            guiElements.playButton.onClick.AddListener(() =>
            {
                if (isMyGame)
                {
                    selectedGame.gameName = guiElements.titleInputField.text;
                    gameRepo.SetData(selectedGameId, selectedGame);
                }

                menuTransitioner.Show("GameController", TransitionDirection.forward);
            });
            guiElements.editButton.onClick.AddListener(() =>
            {
                selectedGame.gameName = guiElements.titleInputField.text;
                gameRepo.SetData(selectedGameId, selectedGame);

                menuTransitioner.Show("CustomizeGameMenu", TransitionDirection.forward);
            });

            guiElements.titleInputField.text = selectedGame.gameName;

            Observable.EveryUpdate()
                .Select(_ => !string.IsNullOrEmpty(guiElements.titleInputField.text))
                .Subscribe(hasTitle => guiElements.playButton.interactable = hasTitle);

            var subcontainer = container.CreateSubContainer();
            subcontainer.BindInstance(characterRepo.GetData(selectedGame.characterId).Value);
            subcontainer.BindInstance(selectedGame);

            var genreAssets = genreCollection.genres.First(genre => genre.genreID == selectedGame.genreId);

            currentPreview = subcontainer.InstantiatePrefab(genreAssets.previewPrefab.gameObject);
            currentPreview.transform.SetParent(guiElements.previewRoot);

            RectTransform rectTransform = currentPreview.GetComponent<RectTransform>();
            rectTransform.anchorMax = Vector2.one;
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.offsetMax = rectTransform.offsetMin = Vector2.zero;
            rectTransform.localScale = Vector3.one;
        }

        private void OnDisable()
        {
            if (currentPreview != null) Destroy(currentPreview);
        }
    }
}
