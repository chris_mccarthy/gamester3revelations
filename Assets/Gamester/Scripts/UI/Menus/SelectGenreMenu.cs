﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gamester.Backend;
using Gamester.Data;
using Gamester.Game;
using GFS.BackendTools;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class SelectGenreMenu : MonoBehaviour
    {
        [System.Serializable]
        private class GUIElements
        {
            public Button faceButton;
            public RawImage faceImage;
            public AspectRatioFitter faceFitter;
            public RectTransform viewPanel;
        }

        [SerializeField]
        private GUIElements guiElements;

        [Inject]
        private MenuTransitionerController transitionController;
        [Inject]
        private GenreCollection genreCollection;
        [Inject]
        private IRepository<CharacterData> characterRepository;
        [Inject]
        private IRepository<CharacterFaceAsset> faceAssetRepository;
        [Inject]
        private IRepository<GameData> gameRepo;
        [Inject]
        private MainMenuState mainMenuState;
        [Inject]
        private DiContainer container;
        [Inject]
        private ClientState clientState;

        private List<GameObject> thumbnails = new List<GameObject>();

        private void Start()
        {
            if (string.IsNullOrEmpty(mainMenuState.selectedCharacterId.Value))
                mainMenuState.selectedCharacterId.Value = characterRepository.Keys.Value.First();

            var regularFaceId = characterRepository.GetData(mainMenuState.selectedCharacterId.Value).Value.faceData.First(face => face.id == "regular").faceAssetId;
            var regularFace = faceAssetRepository.GetData(regularFaceId).Value.faceTexture;
            guiElements.faceImage.texture = regularFace;
            guiElements.faceButton.onClick.AddListener(() => transitionController.Show("CharactersMenu", TransitionDirection.forward));
            guiElements.faceFitter.aspectRatio = (float)regularFace.width / regularFace.height;

            for (int a = 0; a < genreCollection.genres.Length; a++)
            {
                var genreAssets = genreCollection.genres[a];

                var subcontainer = container.CreateSubContainer();

                subcontainer.BindInstance(characterRepository.GetData(mainMenuState.selectedCharacterId.Value).Value);

                GameObject gameThumb = subcontainer.InstantiatePrefab(genreAssets.thumbnailPrefab.gameObject);
                gameThumb.transform.SetParent(guiElements.viewPanel);
                gameThumb.transform.localPosition = Vector3.zero;
                gameThumb.transform.localScale = Vector3.one;
                gameThumb.transform.localRotation = Quaternion.identity;

                thumbnails.Add(gameThumb);

                gameThumb.GetComponentInChildren<Button>().onClick.AddListener(() =>
                {
                    var customizations = new List<CustomizationData>();
                    foreach (var categoryId in genreAssets.customizations.Select(customization => customization.categoryId).Distinct())
                    {
                        var placeholderCustomizationId = genreAssets.customizations.First(customization => customization.categoryId == categoryId).id;
                        customizations.Add(new CustomizationData { id = placeholderCustomizationId, category = categoryId });
                    }

                    var newGameGuid = Guid.NewGuid().ToString();
                    var newGame = new GameData
                    {
                        characterId = mainMenuState.selectedCharacterId.Value,
                        genreId = genreAssets.genreID,
                        gameName = "My Game",
                        id = newGameGuid,
                        customizations = customizations,
                        ownerUserId = clientState.userId.Value
                    };
                    gameRepo.SetData(newGameGuid, newGame);
                    mainMenuState.selectedGameId.Value = newGameGuid;

                    transitionController.Show("PreviewGameMenu", TransitionDirection.forward);
                });
            }
        }

        private void OnDisable()
        {
            foreach (var a in thumbnails)
            {
                Destroy(a);
            }
            thumbnails.Clear();
        }
    }
}
