﻿using Gamester.Backend;
using Gamester.Data;
using GFS.BackendTools;
using Zenject;

namespace Gamester.UI
{
    public class CharactersMenu : AssetListBase<CharacterData>
    {
        [Inject]
        private MainMenuState mainMenuState;
        [Inject]
        private ClientState clientState;
        [Inject]
        private MenuTransitionerController transitioner;

        protected override void ViewElementClicked(string id)
        {
            mainMenuState.selectedCharacterId.Value = id;
            transitioner.Show("SelectGenreMenu", TransitionDirection.forward);
        }

        protected override void AddAssetClicked()
        {
            mainMenuState.selectedCharacterId.Value = string.Empty;
            mainMenuState.selectedFaceId.Value = "regular";
            transitioner.NextMenuId = "SelectGenreMenu";
            transitioner.Show("MakeFaceMenu", TransitionDirection.forward);
        }

        protected override bool FilterAsset(CharacterData asset)
        {
            return asset.ownerId == clientState.userId.Value;
        }

        protected override void HoldAction(string key)
        {
            mainMenuState.selectedCharacterId.Value = key;
            transitioner.Show("CustomizeCharacterMenu", TransitionDirection.forward);
        }
    }
}