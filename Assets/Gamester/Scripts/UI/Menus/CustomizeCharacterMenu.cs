﻿using System.Collections.Generic;
using System.Linq;
using Gamester.Backend;
using Gamester.Data;
using GFS.BackendTools;
using GFS.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class CustomizeCharacterMenu : MonoBehaviour
    {
        [System.Serializable]
        private class UIElements
        {
            public Button deleteButton;
            public Button doneButton;
            public Transform elementsParent;
        }

        [SerializeField] UIElements uiElements;
        [SerializeField] GameObject elementPrefab;
        [SerializeField] string[] faceIds = new string[] { "regular", "sad", "happy" };

        [Inject] MainMenuState mainMenuState;
        [Inject] IRepository<CharacterData> characterDataRepo;
        [Inject] IRepository<CharacterFaceAsset> faceDataRepo;
        [Inject] IRepository<GameData> gameDataRepo;
        [Inject] DiContainer container;
        [Inject] IAlertStrategy alertController;
        [Inject] MenuTransitionerController transitionerController;

        private void Start()
        {
            var characterId = mainMenuState.selectedCharacterId.Value;
            var characterData = characterDataRepo.GetData(characterId).Value;
            foreach (var faceId in faceIds)
            {
                var subcontainer = container.CreateSubContainer();
                subcontainer.BindInstance(characterData);
                var newChildElement = subcontainer.InstantiatePrefab(elementPrefab, uiElements.elementsParent);
                newChildElement.GetComponentInChildren<FaceViewElement>().SetFaceId(faceId);
            }

            uiElements.deleteButton.onClick.AddListener(() =>
            {
                alertController.ShowAlert(new AlertParameters
                {
                    text = "Delete Character?",
                    subtext = "Are you sure you want to delete all games associated with this character?",
                    buttons = new[]
                    {
                        new AlertButton
                        {
                            text = "No"
                        },
                        new AlertButton
                        {
                            text = "Yes",
                            action = () =>
                            {
                                foreach(var faceAssetId in characterData.faceData.Select(data => data.faceAssetId))
                                {
                                    faceDataRepo.DeleteKey(faceAssetId);
                                }

                                foreach(var gameId in new List<string>(gameDataRepo.Keys.Value))
                                {
                                    if(gameDataRepo.GetData(gameId).Value.characterId == characterId)
                                    {
                                        gameDataRepo.DeleteKey(gameId);
                                    }
                                }

                                characterDataRepo.DeleteKey(characterId);

                                transitionerController.Show("CharactersMenu",TransitionDirection.backwards);
                            }
                        }
                    }
                });
            });

            uiElements.doneButton.onClick.AddListener(() =>
            {
                transitionerController.Show("CharactersMenu", TransitionDirection.backwards);
            });
        }
    }
}