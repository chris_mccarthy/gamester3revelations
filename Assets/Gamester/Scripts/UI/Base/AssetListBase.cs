﻿using System.Collections.Generic;
using Gamester.Data;
using Gamester.Utilities;
using GFS.BackendTools;
using GFS.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public abstract class AssetListBase<T> : MonoBehaviour where T : IDependent
    {
        [SerializeField]
        private GameObject viewElementPrefab;
        [SerializeField]
        private GameObject addButtonPrefab;
        [SerializeField]
        private AssetMenuGUIElements guiElements;

        [Inject]
        protected IRepository<T> repo;
        [Inject]
        private DiContainer container;
        [Inject]
        private BuildSettings buildSettings;

        private List<GameObject> viewElements = new List<GameObject>();

        protected bool isStatic = false;
        abstract protected void ViewElementClicked(string id);
        abstract protected void AddAssetClicked();
        abstract protected bool FilterAsset(T asset);
        abstract protected void HoldAction(string key);

        protected virtual void Start()
        {
            repo.Keys
                .First()
                .Subscribe(keys =>
                {
                    // Clear the view
                    foreach (var element in viewElements)
                    {
                        Destroy(element);
                    }
                    viewElements.Clear();

                    // Add the new elements
                    foreach (var key in keys)
                    {
                        var cachedKey = key;
                        var asset = repo.GetData(cachedKey).Value;
                        if (FilterAsset(asset))
                        {
                            var subcontainer = container.CreateSubContainer();
                            subcontainer.BindInstance(asset);

                            var newObject = subcontainer.InstantiatePrefab(viewElementPrefab, guiElements.viewPanel);
                            viewElements.Add(newObject);

                            var button = newObject.GetComponentInChildren<Button>();
                            button.onClick.AddListener(() => ViewElementClicked(cachedKey));

                            if (!isStatic)
                            {
                                var holdButton = newObject.GetComponentInChildren<HoldButton>();
                                holdButton.OnHold
                                    .TakeUntilDestroy(newObject)
                                    .Select(holdTime => holdTime >= buildSettings.buttonHoldTime)
                                    .DistinctUntilChanged()
                                    .Where(isHeldDown => isHeldDown)
                                    .Subscribe(_ => HoldAction(cachedKey));
                            }
                        }
                    }
                    // Add the add button
                    if (!isStatic)
                    {
                        var addButton = container.InstantiatePrefab(addButtonPrefab, guiElements.viewPanel);
                        var button = addButton.GetComponentInChildren<Button>();
                        button.onClick.AddListener(() => AddAssetClicked());
                        viewElements.Add(addButton);
                    }
                });
        }
    }

    [System.Serializable]
    public class AssetMenuGUIElements
    {
        public RectTransform viewPanel;
    }
}