﻿using System.Collections;
using System.Collections.Generic;
using GFS.BackendTools;
using UniRx;
using UnityEngine;
using Zenject;

namespace Gamester.UI
{
    public class LoadingOverlay : MonoBehaviour
    {
        [SerializeField] Transform root;
        [SerializeField] GameObject displayRoot;
        [SerializeField] Transform rotatedObject;
        [SerializeField] float ticksPerSecond = 2;

        [Inject] ClientState clientState;

        private void Start()
        {
            clientState.isLocked
                .TakeUntilDestroy(this)
                .Subscribe(isLocked =>
                {
                    displayRoot.SetActive(isLocked);
                    if (isLocked)
                        root.SetAsLastSibling();
                });

            Observable.EveryUpdate()
                .Where(_ => clientState.isLocked.Value)
                .Subscribe(_ =>
                {
                    rotatedObject.transform.eulerAngles = new Vector3(0, 0, Mathf.Floor(Time.time * ticksPerSecond) * 30);
                });
        }
    }
}