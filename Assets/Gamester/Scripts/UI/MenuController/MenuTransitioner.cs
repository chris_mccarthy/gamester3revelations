﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Gamester.UI
{
    public enum TransitionDirection
    {
        forward,
        backwards
    }

    public class MenuTransitioner : MonoBehaviour
    {
        public class State
        {
            public Vector3 localPosition;
        }

        [SerializeField]
        public Color backgroundColor;
        [SerializeField]
        public bool showHamburger = true;
        [SerializeField]
        public bool isValidBackMenu = true;

        [SerializeField]
        private string id;
        [SerializeField]
        private Transform moveRoot;
        [SerializeField]
        private GameObject toggleRoot;
        [SerializeField]
        private CanvasGroup rootGroup;

        public string ID { get { return id; } }

        public const float TRANSITION_TIME = .25f;
        public const int CANVAS_WIDTH = 800;
        public const int HAMBURGER_OFFSET = 300;

        public bool isTransitioning { get; private set; }

        public State CurrentState
        {
            get
            {
                return new State { localPosition = moveRoot.localPosition };
            }
            set
            {
                moveRoot.position = value.localPosition;
            }
        }

        public void Show(TransitionDirection transition)
        {
            StartShowHideCoroutine(transition, true, CANVAS_WIDTH, 1f);
        }

        public void Hide(TransitionDirection transition)
        {
            StartShowHideCoroutine(transition, false, CANVAS_WIDTH, 1f, Destroy);
        }

        public void EnterHamburger()
        {
            rootGroup.interactable = rootGroup.blocksRaycasts = false;
            StartShowHideCoroutine(TransitionDirection.backwards, false, HAMBURGER_OFFSET, .5f);
        }

        public void ExitHamburger()
        {
            StartShowHideCoroutine(TransitionDirection.forward, true, HAMBURGER_OFFSET, 1f, () => rootGroup.interactable = rootGroup.blocksRaycasts = true);
        }

        public void ShowImmediate()
        {
            moveRoot.localPosition = Vector3.zero;
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        private void StartShowHideCoroutine(TransitionDirection transition, bool show, float tweenDistance, float endAlpha, Action callback = null)
        {
            Vector3 offScreenPoint = Vector3.right * tweenDistance * ((transition == TransitionDirection.backwards) ? 1f : -1f);
            offScreenPoint = offScreenPoint * ((show) ? -1f : 1f);
            Vector3 onScreenPoint = Vector3.zero;

            Vector3 startPosition = (show) ? offScreenPoint : onScreenPoint;
            Vector3 endPosition = (show) ? onScreenPoint : offScreenPoint;

            StartCoroutine(TweenCoroutine(startPosition, endPosition, endAlpha, callback));
        }

        private IEnumerator TweenCoroutine(Vector3 startPosition, Vector3 endPosition, float endAlpha, Action callback)
        {
            isTransitioning = true;
            moveRoot.localPosition = startPosition;
            var currentAlpha = rootGroup.alpha;

            yield return null;

            float startTime = Time.realtimeSinceStartup;
            float endTime = startTime + TRANSITION_TIME;

            while (Time.realtimeSinceStartup < endTime)
            {
                float lerp = (Time.realtimeSinceStartup - startTime) / TRANSITION_TIME;

                moveRoot.localPosition = Vector3.Lerp(startPosition, endPosition, lerp);
                rootGroup.alpha = Mathf.Lerp(currentAlpha, endAlpha, lerp);

                yield return null;
            }

            moveRoot.localPosition = endPosition;

            if (callback != null) callback();
            isTransitioning = false;
        }
    }
}