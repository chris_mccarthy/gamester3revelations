﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.UI
{
    public class MenuTransitionerController : MonoInstaller
    {
        [SerializeField]
        private Image background;
        [SerializeField]
        private string startingMenuID;
        [SerializeField]
        private Transform backgroundRoot;
        [SerializeField]
        private Button backgroundButton;
        [SerializeField]
        private Button hamburgerButton;
        [SerializeField]
        private GameObject hamburgerButtonRoot;
        [SerializeField]
        private MenuTransitioner[] menuControllers;

        public string NextMenuId { get; set; }
        public bool CanGoBack { get; private set; }

        private MenuTransitioner currentMenu;
        private string previousMenuId;

        private void Start()
        {
            currentMenu = InstantiateController(startingMenuID);
            currentMenu.ShowImmediate();
            background.color = currentMenu.backgroundColor;
            hamburgerButtonRoot.SetActive(currentMenu.showHamburger);

            hamburgerButton.onClick.AddListener(EnterHamburger);
        }

        public void Show(string menuID, TransitionDirection transition)
        {
            previousMenuId = currentMenu.ID;
            CanGoBack = currentMenu.isValidBackMenu;

            currentMenu.Hide(transition);
            currentMenu = InstantiateController(menuID);
            currentMenu.Show(transition);

            hamburgerButtonRoot.SetActive(currentMenu.showHamburger);
            StartCoroutine(TweenBackgroundColor(currentMenu.backgroundColor));
        }

        public void EnterHamburger()
        {
            hamburgerButton.targetGraphic.raycastTarget = hamburgerButton.interactable = false;
            currentMenu.EnterHamburger();
            TweenBackgroundMovement(true);
            backgroundButton.onClick.AddListener(ExitHamburger);

            var greyedBackground = Color.Lerp(currentMenu.backgroundColor, Color.black, .2f);
            StartCoroutine(TweenBackgroundColor(greyedBackground));
        }

        public void ExitHamburger(string nextMenu)
        {
            var state = currentMenu.CurrentState;
            currentMenu.Destroy();
            currentMenu = InstantiateController(nextMenu);
            currentMenu.CurrentState = state;

            hamburgerButtonRoot.SetActive(currentMenu.showHamburger);

            ExitHamburger();
        }

        public void ExitHamburger()
        {
            hamburgerButton.targetGraphic.raycastTarget = hamburgerButton.interactable = true;
            StartCoroutine(TweenBackgroundColor(currentMenu.backgroundColor));

            backgroundButton.onClick.RemoveAllListeners();
            currentMenu.ExitHamburger();
            TweenBackgroundMovement(false);
        }

        public void Next()
        {
            Show(NextMenuId, TransitionDirection.forward);
        }

        public void Back()
        {
            Show(previousMenuId, TransitionDirection.backwards);
        }

        public override void InstallBindings()
        {
            Container.BindInstance(this)
                .AsSingle();
        }

        private MenuTransitioner InstantiateController(string id)
        {
            return Container.InstantiatePrefab(menuControllers.FirstOrDefault(a => a.ID == id), transform).GetComponent<MenuTransitioner>();
        }

        private IEnumerator TweenBackgroundColor(Color endColor)
        {
            float totalTime = MenuTransitioner.TRANSITION_TIME;
            float startTime = Time.time;
            float endTime = startTime + totalTime;
            Color startColor = background.color;

            while (Time.time < endTime)
            {
                float lerp = (Time.time - startTime) / totalTime;
                background.color = Color.Lerp(startColor, endColor, lerp);
                yield return null;
            }
            background.color = endColor;
        }

        private void TweenBackgroundMovement(bool show)
        {
            Vector3 offScreenPoint = Vector3.zero;
            //offScreenPoint = offScreenPoint * ((show) ? -1f : 1f);
            Vector3 onScreenPoint = Vector3.right * MenuTransitioner.HAMBURGER_OFFSET;

            Vector3 startPosition = (show) ? offScreenPoint : onScreenPoint;
            Vector3 endPosition = (show) ? onScreenPoint : offScreenPoint;

            StartCoroutine(TweenBackgroundMovement(startPosition, endPosition));
        }

        private IEnumerator TweenBackgroundMovement(Vector3 startPosition, Vector3 endPosition)
        {
            float transitionTime = MenuTransitioner.TRANSITION_TIME;
            backgroundRoot.localPosition = startPosition;

            yield return null;

            float startTime = Time.time;
            float endTime = startTime + transitionTime;

            while (Time.time < endTime)
            {
                float lerp = (Time.time - startTime) / transitionTime;

                backgroundRoot.localPosition = Vector3.Lerp(startPosition, endPosition, lerp);

                yield return null;
            }

            backgroundRoot.localPosition = endPosition;
        }
    }
}