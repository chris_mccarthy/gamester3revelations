﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamester.Game
{
    [CreateAssetMenu(menuName = "Gamester/GameCustomization")]
    public class GameCustomization : ScriptableObject
    {
        public string id;
        public string categoryId;
        public GameObject thumbnail;
        public Object asset;
    }
}
