﻿using UnityEngine;
using Zenject;

namespace Gamester.Game
{
    [CreateAssetMenu(menuName = "Gamester/GenreCollection")]
    public class GenreCollection : ScriptableObjectInstaller
    {
        public GenreAssets[] genres;

        public override void InstallBindings()
        {
            Container.BindInstance(this)
                .AsSingle();
        }
    }
}
