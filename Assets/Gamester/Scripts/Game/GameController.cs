﻿using System;
using System.IO;
using Gamester.Ads;
using Gamester.Backend;
using Gamester.Data;
using Gamester.UI;
using GFS.BackendTools;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Gamester.Game
{
    public class GameController : MonoBehaviour
    {
        [System.Serializable]
        private class PauseMenuUIElements
        {
            public GameObject root;
            public Button resumeButton;
            public Button restartButton;
            public Button homeButton;
            public Button background;
        }

        [System.Serializable]
        private class ContinueGameUIElements
        {
            public GameObject root;
            public Image timingBar;
            public Button watchAdButton;
            public Button giveUpButton;
        }

        [System.Serializable]
        private class EndGameUIElements
        {
            public GameObject root;
            public Text resultsDisplay;
            public Button homeButton;
            public Button restartButton;
        }

        [SerializeField]
        private EndGameUIElements endGameMenu;
        [SerializeField]
        private PauseMenuUIElements pauseMenu;
        [SerializeField]
        private ContinueGameUIElements continueGameUI;
        [SerializeField]
        private MenuTransitioner transitioner;

        private GameState gameState;
        private string genreID;
        private bool continueUsed = false;

        [Inject]
        private MenuTransitionerController menuTransitioner;
        [Inject]
        private BuildSettings buildSettings;
        [Inject]
        private MainMenuState mainMenuState;
        [Inject]
        private IRepository<CharacterData> characterRepo;
        [Inject]
        private IRepository<GameData> gameRepo;
        [Inject]
        private AdsController adsController;
        [Inject]
        private DiContainer container;

        public void Start()
        {
            pauseMenu.root.SetActive(false);
            endGameMenu.root.SetActive(false);

            pauseMenu.background.onClick.AddListener(() =>
            {
                pauseMenu.root.SetActive(false);
                Time.timeScale = 1;

                gameState.isPaused.Value = false;
            });

            pauseMenu.resumeButton.onClick.AddListener(() =>
            {
                pauseMenu.root.SetActive(false);
                Time.timeScale = 1;

                gameState.isPaused.Value = false;
            });

            pauseMenu.restartButton.onClick.AddListener(() =>
            {
                UnloadGame();
                LaunchGame();

                pauseMenu.root.SetActive(false);
                Time.timeScale = 1;

                gameState.isPaused.Value = false;
            });

            pauseMenu.homeButton.onClick.AddListener(() =>
            {
                UnloadGame();
                Time.timeScale = 1;

                menuTransitioner.Show("PreviewGameMenu", TransitionDirection.backwards);
            });

            continueGameUI.watchAdButton.onClick.AddListener(() =>
            {
                continueGameUI.root.SetActive(false);
                adsController.ShowAd()
                    .First()
                    .Subscribe(watchedAd =>
                    {
                        if (watchedAd)
                        {
                            // go back to game
                            Time.timeScale = 1;
                            PlayMakerFSM.BroadcastEvent("Continue");
                        }
                        else
                            endGameMenu.root.SetActive(true);
                    });
            });

            continueGameUI.giveUpButton.onClick.AddListener(() =>
            {
                continueGameUI.root.SetActive(false);

                endGameMenu.resultsDisplay.text = gameState.gameScore.Value;
                endGameMenu.root.SetActive(true);
            });

            endGameMenu.restartButton.onClick.AddListener(() =>
            {
                UnloadGame();
                LaunchGame();
                endGameMenu.root.SetActive(false);
            });

            endGameMenu.homeButton.onClick.AddListener(() =>
            {
                UnloadGame();

                menuTransitioner.Show("PreviewGameMenu", TransitionDirection.backwards);
            });

            Observable.EveryUpdate()
                .First(_ => !transitioner.isTransitioning)
                .Subscribe(_ => LaunchGame());
        }

        private void UnloadGame()
        {
            Time.timeScale = 1f;
            gameState.quitGame.OnNext(Unit.Default);
            gameState.Dispose();
            SceneManager.UnloadSceneAsync(genreID);
        }

        private void LaunchGame()
        {
            continueUsed = false;

            gameState = new GameState();

            var subcontainer = container.CreateSubContainer();
            var selectedGameData = gameRepo.GetData(mainMenuState.selectedGameId.Value).Value;

            subcontainer.BindInstance(characterRepo.GetData(selectedGameData.characterId).Value);
            subcontainer.BindInstance(selectedGameData);
            subcontainer.BindInstance(gameState);

            gameState.isPaused
                .TakeUntilDestroy(this)
                .Subscribe(isPaused =>
                {
                    pauseMenu.root.SetActive(isPaused);
                    Time.timeScale = isPaused ? 0 : 1;
                });

            Observable.FromEvent<UnityAction<Scene, LoadSceneMode>, Scene>(
                handler => (scene, mode) => handler(scene),
                handler => SceneManager.sceneLoaded += handler,
                handler => SceneManager.sceneLoaded -= handler)
                .First()
                .Subscribe(scene =>
                {
                    foreach (var a in scene.GetRootGameObjects())
                    {
                        subcontainer.InjectGameObject(a);
                    }
                });

            var gameData = gameRepo.GetData(mainMenuState.selectedGameId.Value).Value;
            genreID = gameData.genreId;

            Action loadScene = () => SceneManager.LoadScene(genreID, LoadSceneMode.Additive);

            if (buildSettings.useAssetBundles)
            {
                var bundleRequest = AssetBundle.LoadFromFileAsync(Path.Combine(Application.streamingAssetsPath, genreID));

                if (bundleRequest == null)
                    Debug.LogError("Failed to load AssetBundle " + genreID);

                Observable.EveryUpdate()
                        .Select(_ => bundleRequest.isDone)
                        .First(isDone => isDone)
                        .Subscribe(_ => loadScene());
            }
            else
                loadScene();

            gameState.endGame.Subscribe(_ =>
            {
                gameState.isPaused.Value = true;
                if (continueUsed)
                {
                    endGameMenu.root.SetActive(true);
                    endGameMenu.resultsDisplay.text = gameState.gameScore.Value;
                }
                else
                {
                    continueUsed = true;
                    continueGameUI.root.SetActive(true);

                    var totalTime = 5f;
                    var startTime = Time.realtimeSinceStartup;
                    var endTime = Time.realtimeSinceStartup + totalTime;

                    Observable.EveryUpdate()
                        .TakeWhile(__ => continueGameUI.root.activeSelf)
                        .Select(__ => (Time.realtimeSinceStartup - startTime) / totalTime)
                        .Subscribe(lerp =>
                        {
                            if (lerp >= 1)
                            {
                                continueGameUI.root.SetActive(false);
                                gameState.endGame.OnNext(Unit.Default);
                            }
                            else
                                continueGameUI.timingBar.fillAmount = 1f - lerp;
                        });
                }
            });
            gameState.initGame.OnNext(Unit.Default);
        }
    }
}