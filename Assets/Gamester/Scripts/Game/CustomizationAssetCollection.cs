﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamester.Game
{
    [CreateAssetMenu(menuName = "Gamester/CustomizationAssetCollection")]
    public class CustomizationAssetCollection : ScriptableObject
    {
        public KeyedAsset[] assets;
    }

    [System.Serializable]
    public class KeyedAsset
    {
        public string key;
        public Object asset;
    }
}
