﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gamester.Data;
using UniRx;
using UnityEngine;

namespace Gamester.Game
{
    public class GameState : IDisposable
    {
        // Events
        public Subject<Unit> initGame = new Subject<Unit>();
        public Subject<Unit> quitGame = new Subject<Unit>();
        public Subject<Unit> endGame = new Subject<Unit>();

        // States
        public ReactiveProperty<bool> isPaused = new ReactiveProperty<bool>();
        public ReactiveProperty<string> gameScore = new ReactiveProperty<string>();

        public void Dispose()
        {
            initGame.Dispose();
            quitGame.Dispose();
            endGame.Dispose();
            isPaused.Dispose();
            gameScore.Dispose();
        }
    }
}