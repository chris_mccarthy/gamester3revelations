﻿using UnityEngine;

namespace Gamester.Game
{
    [CreateAssetMenu(menuName = "Gamester/GenreAssets")]
    public class GenreAssets : ScriptableObject
    {
        public string genreID;
        public GameObject thumbnailPrefab;
        /// <summary>
        /// This is the previews prefab of the game shown before launching.
        /// This is much like the thumbnail but bigger
        /// Preview prefabs should be:
        ///     - non-interactive
        ///     - light-weight
        ///     - representative of the gameplay
        ///     - using the game faces
        /// </summary>
        public GameObject previewPrefab;
        public GameCustomization[] customizations;
    }
}
