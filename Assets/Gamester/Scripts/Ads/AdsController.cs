﻿using System.Collections;
using System.Collections.Generic;
using Gamester.Data;
using UniRx;
using UnityEngine;
using UnityEngine.Monetization;
using Zenject;

namespace Gamester.Ads
{
    [System.Serializable]
    public class AdsSettings
    {
        public bool adsTestMode;
        public string iOSGameId;
        public string AndroidGameId;
    }

    public class AdsController : MonoInstaller
    {
        [Inject]
        private BuildSettings buildSettings;

        private const string placementId = "rewardedVideo";

        private void Start()
        {
            var settings = buildSettings.adsSettings;
            string gameId = (Application.platform == RuntimePlatform.IPhonePlayer) ? settings.iOSGameId : settings.AndroidGameId;

            if (Monetization.isSupported)
            {
                Monetization.Initialize(gameId, settings.adsTestMode);
            }
            else
            {
                Debug.LogError("Monetization is not supported! Ads will not work.");
            }
        }

        public IObservable<bool> ShowAd()
        {
            Subject<bool> returned = new Subject<bool>();

            if (!Monetization.IsReady(placementId))
            {
                returned.OnNext(false);
                Debug.Log("Ad is not ready");
            }
            else
            {
                ShowAdPlacementContent ad = Monetization.GetPlacementContent(placementId) as ShowAdPlacementContent;

                ad.Show(new ShowAdCallbacks
                {
                    finishCallback = result =>
                    {
                        if (result == ShowResult.Finished)
                        {
                            returned.OnNext(true);
                        }
                        else if (result == ShowResult.Skipped)
                        {
                            Debug.Log("The player skipped the video - DO NOT REWARD!");
                            returned.OnNext(false);
                        }
                        else if (result == ShowResult.Failed)
                        {
                            Debug.LogError("Video failed to show");
                            returned.OnNext(false);
                        }
                    }
                });
            }

            return returned;
        }

        public override void InstallBindings()
        {
            Container.BindInstance(this);
        }
    }
}