﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Prime31;

namespace Gamester.Photo
{
    public class CameraRollLoader : MonoBehaviour
    {
        [System.Serializable]
        private class GUIElements
        {
            public Transform rotationRoot;
            public RawImage photoCanvas;
            public AspectRatioFitter fitter;
        }

        [SerializeField]
        private GUIElements guiElements;

        public void LoadCameraRollPhoto(System.Action<bool> callback)
        {
            StartCoroutine(LoadCameraRollPhotoCoroutine((bool wasSuccesful, Texture photoTexture) =>
            {
                if (wasSuccesful)
                {
                    guiElements.photoCanvas.texture = photoTexture;
                    guiElements.fitter.aspectRatio = ((float)photoTexture.width) / ((float)photoTexture.height);
                    guiElements.rotationRoot.localRotation = Quaternion.identity;
                }
                callback(wasSuccesful);
            }));
        }

#if UNITY_IPHONE
	private IEnumerator LoadCameraRollPhotoCoroutine(System.Action<bool,Texture> callback){
		bool photoSelected = false;
		string photoFilePath = null;
		bool receivedCallback = false;

		System.Action clearListeners = null;
		System.Action handleCancelEvent = () => {
			clearListeners();
		};
		System.Action<string> handleChoseEvent = (string imagePath) => {
			photoFilePath = imagePath;
			photoSelected = true;
			clearListeners();
		};
		clearListeners = () => {
			EtceteraManager.imagePickerCancelledEvent -= handleCancelEvent;
			EtceteraManager.imagePickerChoseImageEvent -= handleChoseEvent;
			receivedCallback = true;
		};

		EtceteraManager.imagePickerCancelledEvent += handleCancelEvent;
		EtceteraManager.imagePickerChoseImageEvent += handleChoseEvent;

		EtceteraBinding.promptForPhoto( 0.25f, PhotoPromptType.Album );

		while(!receivedCallback){
			yield return null;
		}

		if(photoSelected){
			string path = "file://"+ photoFilePath;
			WWW loader = new WWW(path);
			while(!loader.isDone){
				yield return null;
			}

			if(string.IsNullOrEmpty(loader.error)){
				callback(true,loader.texture);
			}else{
				Debug.LogError("Error while loading from camera roll photo.\nPath: "+path+"\n"+loader.error);
				callback(false,null);
			}
		}else{
			callback(false,null);
		}
    }
#else
        private IEnumerator LoadCameraRollPhotoCoroutine(System.Action<bool, Texture> callback)
        {
            yield return null;
            callback(false, null);
        }
#endif
    }
}