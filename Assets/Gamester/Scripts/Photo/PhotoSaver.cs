﻿using System;
using System.Collections;
using Gamester.Data;
using Gamester.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gamester.Photo
{
    public class PhotoSaver : MonoInstaller
    {
        private const float textureResizeMultiplier = 1;

        [System.Serializable]
        public class SavePhotoParams
        {
            public RawImage mask;
        }

        public void SavePhoto(SavePhotoParams param, Action<Texture2D> callback)
        {
            StartCoroutine(SavePhotoCoroutine(param, callback));
        }

        private IEnumerator SavePhotoCoroutine(SavePhotoParams param, Action<Texture2D> callback)
        {
            Texture2D copiedTexture = param.mask.texture as Texture2D;
            Texture2D resizedMask = new Texture2D(copiedTexture.width, copiedTexture.height);

            Graphics.CopyTexture(copiedTexture, resizedMask);

            Rect screenshotRect = GetViewRect(param.mask.rectTransform);

            resizedMask.Scale((int)screenshotRect.width, (int)screenshotRect.height, false);
            resizedMask.Apply();

            var maskPhoto = resizedMask;

            yield return new WaitForEndOfFrame();

            Texture2D screenshotTexture = new Texture2D((int)screenshotRect.width, (int)screenshotRect.height);

            screenshotTexture.ReadPixels(screenshotRect, 0, 0);

            Color[] pixels = screenshotTexture.GetPixels();
            Color[] maskPixels = maskPhoto.GetPixels();

            for (int a = 0; a < pixels.Length; a++)
            {
                var color = pixels[a];
                color.a = maskPixels[a].a;// TODO null here
                pixels[a] = color;
            }
            screenshotTexture.SetPixels(pixels);

            screenshotTexture.Scale(CharacterFaceAsset.FixedWidth, CharacterFaceAsset.FixedHeight, false);

            screenshotTexture.Compress(true);

            screenshotTexture.Apply();

            yield return null;

            callback(screenshotTexture);
        }

        private Rect GetViewRect(RectTransform rectTransform)
        {
            Vector3[] corners = new Vector3[4];
            rectTransform.GetWorldCorners(corners);
            Vector3 min = Vector3.one * Mathf.Infinity;
            Vector3 max = -Vector3.one * Mathf.Infinity;
            foreach (Vector3 corner in corners)
            {
                if (corner.x < min.x || corner.y < min.y)
                {
                    min = corner;
                }
                if (corner.x > max.x || corner.y > max.y)
                {
                    max = corner;
                }
            }

            return Rect.MinMaxRect(min.x, min.y, max.x, max.y);
        }

        public override void InstallBindings()
        {
            Container.BindInstance(this)
                .AsSingle();
        }
    }
}