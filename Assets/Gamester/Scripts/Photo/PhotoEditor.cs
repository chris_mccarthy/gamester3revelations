﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamester.Photo
{
    public class PhotoEditor : MonoBehaviour
    {
        [SerializeField]
        private Transform target;

        private Transform scalingTransform;
        private Vector3 targetStartPosition;
        private Quaternion targetStartRotation;
        private Vector3 targetStartScale;

        public void Start()
        {
            scalingTransform = (new GameObject("Scaling Transform")).transform;
        }

        public void Activate()
        {
            targetStartPosition = target.localPosition;
            targetStartRotation = target.localRotation;
            targetStartScale = target.localScale;
            StartCoroutine(DetectFingerCoroutine());
        }

        public void Reset()
        {
            target.localPosition = targetStartPosition;
            target.localRotation = targetStartRotation;
            target.localScale = targetStartScale;
        }

        public void Deactivate()
        {
            StopAllCoroutines();
        }

        private IEnumerator DetectFingerCoroutine()
        {
            while (true)
            {
                if (Input.touchCount == 1)
                {
                    Touch touch = Input.GetTouch(0);
                    if (touch.phase == TouchPhase.Began)
                    {
                        StartCoroutine(DragEditCoroutine(touch));
                    }
                }
                else if (Input.touchCount == 2)
                {
                    Touch t1 = Input.GetTouch(0);
                    Touch t2 = Input.GetTouch(1);
                    if (t1.phase == TouchPhase.Began || t2.phase == TouchPhase.Began)
                    {
                        StartCoroutine(PinchEditCoroutine(t1, t2));
                    }
                }

                yield return null;
            }
        }

        private IEnumerator DragEditCoroutine(Touch touch)
        {
            Vector3 targetStartPosition = target.position;
            Vector3 startPosition = touch.position;

            while (Input.touchCount >= 1 && Input.GetTouch(0).fingerId == touch.fingerId)
            {
                Vector3 newTouchPosition = Input.GetTouch(0).position;

                target.position = targetStartPosition + (newTouchPosition - startPosition);

                yield return null;
            }
        }

        private IEnumerator PinchEditCoroutine(Touch touch1, Touch touch2)
        {
            Transform previousParent = target.parent;

            Vector3 t1Start = touch1.position;
            Vector3 t2Start = touch2.position;

            float startDistance = Vector3.Distance(t1Start, t2Start);
            float startRotation = Mathf.Atan2(t1Start.y - t2Start.y, t1Start.x - t2Start.x);
            Vector3 startMidPoint = Vector3.Lerp(t1Start, t2Start, .5f);

            scalingTransform.SetParent(previousParent);

            scalingTransform.position = startMidPoint;
            scalingTransform.rotation = Quaternion.identity;
            scalingTransform.localScale = Vector3.one;

            target.SetParent(scalingTransform);

            Vector3 baseScale = scalingTransform.localScale;

            while (Input.touchCount == 2)
            {
                Vector3 p1 = new Vector3();
                Vector3 p2 = new Vector3();

                foreach (Touch touch in Input.touches)
                {
                    if (touch.fingerId == touch1.fingerId)
                    {
                        p1 = touch.position;
                    }
                    if (touch.fingerId == touch2.fingerId)
                    {
                        p2 = touch.position;
                    }
                }

                // Apply Move
                Vector3 newMidPoint = Vector3.Lerp(p1, p2, .5f);

                scalingTransform.position = startMidPoint + (newMidPoint - startMidPoint);

                // Apply Scale
                float scale = Vector3.Distance(p1, p2) / startDistance;

                scalingTransform.localScale = baseScale * (scale);

                // Apply Rotate
                float newRotation = Mathf.Atan2(p1.y - p2.y, p1.x - p2.x);

                scalingTransform.eulerAngles = Vector3.forward * ((newRotation - startRotation) * Mathf.Rad2Deg);

                yield return null;
            }

            target.SetParent(previousParent);
        }

        private void OnDestroy()
        {
            if (scalingTransform != null) Destroy(scalingTransform.gameObject);
        }
    }
}