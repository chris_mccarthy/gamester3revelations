﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Gamester.Photo
{
    public class PhotoTaker : MonoBehaviour
    {

        [System.Serializable]
        private class PhotoTakerGUIElements
        {
            public RawImage cameraImage;
            public Transform rotationRoot;
            public Transform reflectionRoot;
            public AspectRatioFitter aspectRatioFitter;
        }

        [SerializeField]
        private PhotoTakerGUIElements guiElements;

        private WebCamDevice frontCamera;
        private WebCamDevice backCamera;
        private WebCamTexture webcamTexture = null;
        private bool usingFrontCamera;

        private void OnEnable()
        {
            if (WebCamTexture.devices.Length > 0)
            {
                foreach (WebCamDevice device in WebCamTexture.devices)
                {
                    if (device.isFrontFacing)
                    {
                        frontCamera = device;
                    }
                    else
                    {
                        backCamera = device;
                    }
                }
            }
            else
            {
                Debug.LogError("No webcam devices found");
            }

            usingFrontCamera = true;
            ActivateCamera(usingFrontCamera);
        }

        public void FlipCamera()
        {
            usingFrontCamera = !usingFrontCamera;
            ActivateCamera(usingFrontCamera);
        }

        public void ActivateCamera()
        {
            ActivateCamera(usingFrontCamera);
        }

        private void ActivateCamera(bool useFrontCamera)
        {
            usingFrontCamera = useFrontCamera;
            StartCoroutine(ActivateCameraCoroutine(usingFrontCamera));
        }

        private IEnumerator ActivateCameraCoroutine(bool useFrontCamera)
        {
            if (webcamTexture != null)
            {
                webcamTexture.Stop();
            }

            WebCamDevice device = (useFrontCamera) ? frontCamera : backCamera;

            webcamTexture = new WebCamTexture(device.name);
            webcamTexture.filterMode = FilterMode.Trilinear;

            webcamTexture.Play();

            while (webcamTexture.width < 50)
            {
                yield return null;
            }

            // Scale as necessary
            Vector3 localScale = guiElements.reflectionRoot.localScale;
            localScale.x = Mathf.Abs(localScale.x) * ((useFrontCamera) ? -1 : 1);
            localScale.y = Mathf.Abs(localScale.y) * ((webcamTexture.videoVerticallyMirrored) ? -1 : 1);
            guiElements.reflectionRoot.localScale = localScale;

            Debug.Log("Camera name: " + device.name + "\nisFrontFacing: " + device.isFrontFacing + "\nvideoRotationAngle: " + webcamTexture.videoRotationAngle + "\nvideoVerticallyMirrored: " + webcamTexture.videoVerticallyMirrored);

            guiElements.aspectRatioFitter.aspectRatio = ((float)webcamTexture.width) / ((float)webcamTexture.height);
            guiElements.rotationRoot.localEulerAngles = Vector3.forward * webcamTexture.videoRotationAngle;

            guiElements.cameraImage.texture = webcamTexture;
        }

        public Texture TakePhoto()
        {
            Texture2D newTexture = new Texture2D(webcamTexture.width, webcamTexture.height);
            newTexture.name = "PhotoTexture";
            newTexture.SetPixels(webcamTexture.GetPixels());
            newTexture.Apply();
            return newTexture;
        }

        public void ResetCanvasTransforms()
        {
            guiElements.rotationRoot.localRotation = Quaternion.identity;
            guiElements.reflectionRoot.localScale = Vector3.one;
        }

        private void OnDisable()
        {
            webcamTexture.Stop();
        }
    }
}