﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;

namespace Gamester.Backend
{
    public class MainMenuState : MonoInstaller
    {
        public ReactiveProperty<string> selectedGameId = new ReactiveProperty<string>();
        public ReactiveProperty<string> selectedCharacterId = new ReactiveProperty<string>();
        public ReactiveProperty<string> selectedFaceId = new ReactiveProperty<string>();

        public override void InstallBindings()
        {
            Container.BindInstance(this)
                .AsSingle();
        }
    }
}