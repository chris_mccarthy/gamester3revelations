﻿using System;
using Gamester.Data;
using GFS.BackendTools;
using Zenject;

namespace Gamester.Backend
{
    public class RepositoryInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<SaveToCloudRepository<CharacterData, string>>()
                .AsSingle()
                .WithArguments("CharacterData", new JSONSerializer<CharacterData>(), new SaveStringCloudStrategy(), true)
                .NonLazy();

            Container.BindInterfacesAndSelfTo<SaveToCloudRepository<CharacterFaceAsset, byte[]>>()
                .AsSingle()
                .WithArguments("CharacterFaceAsset", new CharacterFaceAssetSerializer(), new SaveBinaryCloudStrategy(), true)
                .NonLazy();

            Container.BindInterfacesAndSelfTo<SaveToCloudRepository<GameData, string>>()
                .AsSingle()
                .WithArguments("GameData", new JSONSerializer<GameData>(), new SaveStringCloudStrategy(), true)
                .NonLazy();
        }


        /// <summary>
        /// DO NOT USE!    
        /// This script is only so the code wont be generated at runtime. See here for explaination:
        /// https://docs.unity3d.com/Manual/ScriptingRestrictions.html
        /// </summary>
        private void AOT()
        {
            var repo3 = new SaveToCloudRepository<CharacterData, string>("Whatever0", new JSONSerializer<CharacterData>(), new SaveStringCloudStrategy(), true);
            var repo = new SaveToCloudRepository<CharacterFaceAsset, byte[]>("Whatever1", new CharacterFaceAssetSerializer(), new SaveBinaryCloudStrategy(), true);
            var repo2 = new SaveToCloudRepository<GameData, string>("Whatever2", new JSONSerializer<GameData>(), new SaveStringCloudStrategy(), true);

            // Include an exception so we can be sure to know if this method is ever called.
            throw new InvalidOperationException("This method is used for AOT code generation only. Do not call it at runtime.");

        }
    }
}