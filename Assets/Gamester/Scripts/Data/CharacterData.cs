﻿using System;
using System.Collections.Generic;
using GFS.BackendTools;
using GFS.Utilities;
using UnityEngine;

namespace Gamester.Data
{
    [System.Serializable]
    public class CharacterData : IOwnedData, IDependent
    {

        public string id;
        public string ownerId;
        public List<CharacterFaceData> faceData;

        public string OwnerId
        {
            get
            {
                return ownerId;
            }
        }

        public IEnumerable<IDependency> GetDependencies()
        {
            foreach (var face in faceData)
            {
                yield return new Dependency
                {
                    id = face.faceAssetId,
                    repositoryType = typeof(IRepository<CharacterFaceAsset>)
                };
            }
        }
    }

    [System.Serializable]
    public class CharacterFaceData
    {
        public string id;
        public string faceAssetId;
    }

    public class CharacterFaceAsset : IDependent
    {
        public const int FixedWidth = 131;
        public const int FixedHeight = 195;

        public Texture2D faceTexture;

        public IEnumerable<IDependency> GetDependencies()
        {
            return new List<IDependency>();
        }
    }

    public class CharacterFaceAssetSerializer : IDataSerializer<CharacterFaceAsset, byte[]>
    {
        public CharacterFaceAsset DeserializeData(byte[] data)
        {
            var tex = new Texture2D(CharacterFaceAsset.FixedWidth, CharacterFaceAsset.FixedHeight, TextureFormat.DXT5, false);
            tex.LoadRawTextureData(data);
            tex.Apply();
            return new CharacterFaceAsset { faceTexture = tex };
        }

        public byte[] SerializeData(CharacterFaceAsset data)
        {
            var bytes = data.faceTexture.GetRawTextureData();
            return bytes;
        }
    }
}
