﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamester.Data
{
    [System.Serializable]
    public struct CustomizationData
    {
        public string category;
        public string id;
    }
}
