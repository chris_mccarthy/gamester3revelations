﻿using System.Collections.Generic;
using GFS.BackendTools;
using GFS.Utilities;

namespace Gamester.Data
{
    [System.Serializable]
    public class GameData : IOwnedData, IDependent
    {
        public string id;
        public string ownerUserId;
        public string characterId;
        public string genreId;
        public string gameName;
        public List<CustomizationData> customizations;

        public string OwnerId
        {
            get
            {
                return ownerUserId;
            }
        }

        public GameData() { }

        public GameData(GameData gameData)
        {
            id = gameData.id;
            ownerUserId = gameData.ownerUserId;
            characterId = gameData.characterId;
            genreId = gameData.genreId;
            gameName = gameData.gameName;
            customizations = new List<CustomizationData>(gameData.customizations);
        }

        public IEnumerable<IDependency> GetDependencies()
        {
            yield return new Dependency
            {
                id = characterId,
                repositoryType = typeof(IRepository<CharacterData>)
            };
        }
    }
}
