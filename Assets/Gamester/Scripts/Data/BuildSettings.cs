﻿using System.Collections;
using System.Collections.Generic;
using Gamester.Ads;
using UnityEngine;
using Zenject;

namespace Gamester.Data
{
    public class BuildSettings : MonoInstaller
    {
        [SerializeField]
        public bool useAssetBundles;
        [SerializeField]
        // TODO: Move this to the title data
        public string privacyPolicyURL = "https://www.giantfoxstudios.com/privacy";
        [SerializeField]
        public float buttonHoldTime = 2f;
        // TODO: Move this to the title data
        [SerializeField]
        public AdsSettings adsSettings;

        public override void InstallBindings()
        {
            Container.BindInstance(this)
                .AsSingle();
        }
    }
}