﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Gamester.Utilities
{
    public class HoldButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        private Subject<float> onHold = new Subject<float>();
        private CompositeDisposable holdDisposable = new CompositeDisposable();

        public IObservable<float> OnHold
        {
            get
            {
                return onHold;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            float holdStartTime = Time.time;
            Observable.EveryUpdate()
                .Select(_ => Time.time - holdStartTime)
                .Subscribe(delta => onHold.OnNext(delta))
                .AddTo(holdDisposable);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            holdDisposable.Clear();
        }
    }
}