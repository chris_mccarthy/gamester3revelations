﻿using System;
using System.Linq;
using GFS.BackendTools;
using UniRx;

namespace GFS.Utilities
{
    public interface IOwnedData
    {
        string OwnerId { get; }
    }

    public static class RepositoryUtils
    {
        public static bool HasKeys<T>(this IRepository<T> repository, string userId) where T : IOwnedData, IDependent
        {
            return repository.Keys.Value.Count(key => repository.GetData(key).Value.OwnerId == userId) > 0;
        }
    }

    public class CustomReadOnlyReactiveProperty<I, T> : IReadOnlyReactiveProperty<T>
    {
        public IReadOnlyReactiveProperty<I> property;
        public Func<I, T> mutateValue;

        public T Value
        {
            get
            {
                return mutateValue(property.Value);
            }
        }

        public bool HasValue
        {
            get
            {
                return true;
            }
            set { }
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            return property.Select(mutateValue).Subscribe(observer);
        }
    }
}