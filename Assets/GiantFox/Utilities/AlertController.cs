﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Prime31;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.Utilities
{
    public struct AlertParameters
    {
        public string text;
        public string subtext;
        public AlertButton[] buttons;
    }

    public struct AlertButton
    {
        public string text;
        public Action action;
    }

    public interface IAlertStrategy
    {
        void ShowAlert(AlertParameters param);
    }

    public class AlertController : MonoInstaller, IAlertStrategy
    {
        public void ShowAlert(AlertParameters param)
        {
#if UNITY_EDITOR
            param.buttons.First(button => button.action != null).action();
#else
            EtceteraBinding.showAlertWithTitleMessageAndButtons(param.text, param.subtext, param.buttons.Select(button => button.text).ToArray());

            Observable.FromEvent<string>(handler => EtceteraManager.alertButtonClickedEvent += handler, handler => EtceteraManager.alertButtonClickedEvent -= handler)
                .First()
                .Subscribe(text =>
                {
                    var action = param.buttons.FirstOrDefault(button => button.text == text).action;
                    if (action != null)
                        action();
                });
#endif
        }

        public override void InstallBindings()
        {
            Container.Bind<IAlertStrategy>()
                .To<AlertController>()
                .FromInstance(this)
                .NonLazy();
        }
    }
}