﻿using System.Collections.Generic;
using PlayerIOClient;
using UniRx;

namespace GFS.BackendTools
{
    public class ClientState
    {
        public ReactiveProperty<bool> isLocked = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> isLoggedIn = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> isFBLinked = new ReactiveProperty<bool>(false);
        public ReactiveProperty<bool> isCloudLoggedIn = new ReactiveProperty<bool>(false);
        public ReactiveProperty<string> userId = new ReactiveProperty<string>(null);
        public ReactiveProperty<string> fbUserId = new ReactiveProperty<string>(null);
        public ReactiveProperty<Client> cloudClient = new ReactiveProperty<Client>(null);
        public ReactiveProperty<Dictionary<string, string>> titleData = new ReactiveProperty<Dictionary<string, string>>(new Dictionary<string, string>());

        public void Reset()
        {
            isLoggedIn.Value = false;
            isFBLinked.Value = false;
            isCloudLoggedIn.Value = false;
            userId.Value = string.Empty;
            fbUserId.Value = string.Empty;
            cloudClient.Value = null;
            titleData.Value = new Dictionary<string, string>();
        }
    }
}