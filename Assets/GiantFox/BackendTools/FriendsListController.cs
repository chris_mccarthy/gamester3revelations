﻿using PlayFab;
using PlayFab.ClientModels;
using SimpleJSON;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    /// <summary>
    /// This manages your friendslist and the assets related to it
    /// </summary>
    public class FriendsListController : IInitializable
    {
        [Inject] ClientState clientState;
        [Inject] CloudController cloudController;

        private Subject<string> friends = new Subject<string>();

        public void Initialize()
        {
            clientState.isFBLinked
                .CombineLatest(clientState.isCloudLoggedIn, (fbLinked, cloudLoggedIn) => fbLinked && cloudLoggedIn)
                .DistinctUntilChanged()
                .Where(fbLinked => fbLinked)
                .Subscribe(_ =>
                {
                    PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
                    {
                        FunctionName = "getFriends",
                        GeneratePlayStreamEvent = true,
                    },
                    results =>
                    {
                        Debug.Log("Friendslist: " + results.FunctionResult);
                        if (results.FunctionResult != null)
                        {
                            var jsonObject = JSON.Parse(results.FunctionResult.ToString());
                            foreach (var friendData in jsonObject["Friends"].AsArray)
                            {
                                var friendId = friendData.Value["FriendPlayFabId"];

                                friends.OnNext(friendId);
                            }
                        }
                    },
                    error => Debug.LogError(error.GenerateErrorReport()));
                });
        }

        public IObservable<string> Friends
        {
            get
            {
                return friends;
            }
        }
    }
}