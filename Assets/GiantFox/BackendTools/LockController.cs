﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;
using System.Linq;

namespace GFS.BackendTools
{
    public interface ILockController
    {
        string Lock();
        void Unlock(string lockToken);
    }

    public class LockController : ILockController, IDisposable, IInitializable
    {
        [Inject] ClientState clientState;

        private class LockToken
        {
            public string guid;
            public string stack;

            public override string ToString()
            {
                return stack;
            }
        }

        private List<LockToken> lockTokens = new List<LockToken>();
        private CompositeDisposable disposables = new CompositeDisposable();

        public void Initialize()
        {
            Observable.EveryUpdate()
                .Subscribe(_ => RefreshLock())
                .AddTo(disposables);
        }

        public void Dispose()
        {
            disposables.Dispose();
        }

        public string Lock()
        {
            var guid = Guid.NewGuid().ToString();
            lockTokens.Add(new LockToken
            {
                guid = guid,
                stack = Environment.StackTrace
            });
            RefreshLock();
            return guid;
        }

        public void Unlock(string guid)
        {
            lockTokens.Remove(lockTokens.First(lockToken => lockToken.guid == guid));
            RefreshLock();
        }

        private void RefreshLock()
        {
            clientState.isLocked.Value = lockTokens.Count > 0;
            /*foreach (var token in lockTokens)
            {
                Debug.Log(token);
            }*/
        }
    }
}