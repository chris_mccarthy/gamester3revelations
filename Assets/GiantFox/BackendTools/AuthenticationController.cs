﻿using System.Collections.Generic;
using Facebook.Unity;
using GFS.Utilities;
using PlayerIOClient;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    /// <summary>
    /// This handles logging into three services:
    ///     - Playfab which handles tracking player's data and serves as a frictionless way to authenticate the user
    ///     - Facebook which is used for all social interactions via the friends list
    ///     - Player.IO which is used as a database cloud service.
    /// </summary>
    public class AuthenticationController : IInitializable
    {
        [Inject] ILockController lockController;
        [Inject] ClientState clientState;
        [Inject] IAlertStrategy alertStrategy;

        private string playerIOGameId;

        public AuthenticationController(string playerIOGameId)
        {
            this.playerIOGameId = playerIOGameId;
        }

        public void Initialize()
        {
            FrictionlessLogin();

            // Log into the cloud with whatever userId is current
            clientState.userId
                .DistinctUntilChanged()
                .Where(userId => !string.IsNullOrEmpty(userId))
                .Subscribe(userId =>
                {
                    var lockToken = lockController.Lock();

                    PlayerIO.Authenticate(
                        playerIOGameId,
                        "public",
                        new Dictionary<string, string> {
                            { "userId", userId },
                        },
                        null,
                        newClient =>
                        {
                            Debug.Log("Logged in with Player.IO");
                            clientState.cloudClient.Value = newClient;
                            clientState.isCloudLoggedIn.Value = true;
                            lockController.Unlock(lockToken);
                        },
                        error =>
                        {
                            Debug.LogError(error.Message);
                            lockController.Unlock(lockToken);
                        });
                });
        }

        public void LoginToFacebook()
        {
            var lockToken = lockController.Lock();

            FB.LogInWithReadPermissions(new[] { "user_friends" }, result =>
            {
                if (result == null || string.IsNullOrEmpty(result.Error))
                {
                    Debug.Log("Facebook Auth Complete! Access Token: " + AccessToken.CurrentAccessToken.TokenString + "\nLogging into PlayFab...");

                    LinkFacebook(AccessToken.CurrentAccessToken.TokenString, false);
                }
                else
                {
                    Debug.Log("Facebook Auth Failed: " + result.Error + "\n" + result.RawResult);
                }
                lockController.Unlock(lockToken);
            });
        }

        public void LogoutFacebook()
        {
            Debug.Log("Logging out of fb");

            clientState.Reset();
            var lockToken = lockController.Lock();

            FB.LogOut();

            var request = new UnlinkIOSDeviceIDRequest
            {
                DeviceId = SystemInfo.deviceUniqueIdentifier
            };

            PlayFabClientAPI.UnlinkIOSDeviceID(request,
            result =>
            {
                Debug.Log("Device unlinked!");
                lockController.Unlock(lockToken);
                FrictionlessLogin();
            },
            error =>
            {
                Debug.LogError(error.GenerateErrorReport());
                lockController.Unlock(lockToken);
            });
        }

        private void FrictionlessLogin()
        {
            Debug.Log("Starting frictionless login");

            {
                var lockToken = lockController.Lock();
                // Log in with playfab using the device as credentials
                var request = new LoginWithIOSDeviceIDRequest
                {
                    CreateAccount = true,
                    DeviceId = SystemInfo.deviceUniqueIdentifier,
                    DeviceModel = SystemInfo.deviceModel
                };

                PlayFabClientAPI.LoginWithIOSDeviceID(request,
                    result =>
                    {
                        Debug.Log("Frictionless login successful");
                        clientState.isLoggedIn.Value = true;
                        RefreshAccountInfo();
                        lockController.Unlock(lockToken);
                    },
                    error =>
                    {
                        Debug.LogError(error.GenerateErrorReport());
                        lockController.Unlock(lockToken);
                    });
            }

            {
                var lockToken = lockController.Lock();
                // Initialize fb and renew the token if necessary
                if (!FB.IsInitialized)
                {
                    FB.Init(() =>
                    {
                        if (FB.IsLoggedIn)
                        {
                            FB.Mobile.RefreshCurrentAccessToken(result =>
                            {
                                Debug.Log("FB initialized, Account token refreshed");
                                lockController.Unlock(lockToken);
                            });
                        }
                        else
                        {
                            Debug.Log("FB initialized");
                            lockController.Unlock(lockToken);
                        }
                    });
                }
                else
                {
                    lockController.Unlock(lockToken);
                }
            }
        }

        private void LinkFacebook(string token, bool force)
        {
            var lockToken = lockController.Lock();

            var request = new LinkFacebookAccountRequest
            {
                AccessToken = token,
                ForceLink = force
            };

            PlayFabClientAPI.LinkFacebookAccount(request, _ =>
            {
                Debug.Log("FB account linked");

                clientState.isFBLinked.Value = true;
                lockController.Unlock(lockToken);

                RefreshAccountInfo();
            }, error =>
            {
                Debug.LogError(error.GenerateErrorReport());

                // If the account is already linked, offer the user to log into that
                if (error.Error.ToString() == "LinkedAccountAlreadyClaimed")
                {
                    alertStrategy.ShowAlert(new AlertParameters
                    {
                        text = "Account already exists!",
                        subtext = "This Facebook account is already linked to an account. Do you want to log into it?",
                        buttons = new AlertButton[]
                        {
                            new AlertButton
                            {
                                text = "No",
                                action = () => LinkFacebook(token, true)
                            },
                            new AlertButton
                            {
                                text = "Yes",
                                action = () => LoginToPlayfabWithFacebook(token)
                            }
                        }
                    });
                }

                lockController.Unlock(lockToken);
            });
        }

        private void LoginToPlayfabWithFacebook(string token)
        {
            Debug.Log("Logging into playfab using fb Id");
            var lockToken = lockController.Lock();

            clientState.Reset();

            var request = new LoginWithFacebookRequest
            {
                AccessToken = token
            };
            PlayFabClientAPI.LoginWithFacebook(request, result =>
            {
                var linkRequest = new LinkIOSDeviceIDRequest
                {
                    ForceLink = true,
                    DeviceId = SystemInfo.deviceUniqueIdentifier,
                    DeviceModel = SystemInfo.deviceModel
                };
                PlayFabClientAPI.LinkIOSDeviceID(linkRequest,
                    linkResult =>
                    {
                        clientState.isLoggedIn.Value = true;
                        clientState.isFBLinked.Value = true;
                        lockController.Unlock(lockToken);
                        RefreshAccountInfo();
                    },
                    error =>
                    {
                        Debug.LogError(error.GenerateErrorReport());

                        clientState.isLoggedIn.Value = true;
                        clientState.isFBLinked.Value = true;
                        lockController.Unlock(lockToken);
                    });
            },
            error =>
            {
                Debug.LogError(error.GenerateErrorReport());
                lockController.Unlock(lockToken);
            });
        }

        private void RefreshAccountInfo()
        {
            Debug.Log("RefreshAccountInfo()");
            PlayFabClientAPI.GetAccountInfo(new GetAccountInfoRequest(),
                results =>
                {
                    Debug.Log("Account info refreshed");
                    clientState.userId.Value = results.AccountInfo.PlayFabId;
                    clientState.isFBLinked.Value = results.AccountInfo.FacebookInfo != null;
                    clientState.fbUserId.Value = (results.AccountInfo.FacebookInfo != null) ? results.AccountInfo.FacebookInfo.FacebookId : string.Empty;
                },
                error => Debug.LogError(error.GenerateErrorReport()));
        }
    }
}