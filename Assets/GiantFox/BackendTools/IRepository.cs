﻿using System.Collections.Generic;
using System.Text;
using UniRx;
using UnityEngine;

namespace GFS.BackendTools
{
    public interface IRepositoryBase
    {
        IReadOnlyReactiveProperty<List<string>> Keys { get; }
        IReadOnlyReactiveProperty<IDependent> GetDependent(string key);
    }

    public interface IRepository<T> : IRepositoryBase where T : IDependent
    {
        IReadOnlyReactiveProperty<T> GetData(string key);
        void SetData(string key, T newData);
        void DeleteKey(string key);
    }

    public interface IDataSerializer<T, D>
    {
        D SerializeData(T data);
        T DeserializeData(D data);
    }

    public class JSONSerializer<T> : IDataSerializer<T, string>
    {
        public T DeserializeData(string data)
        {
            return JsonUtility.FromJson<T>(data);
        }

        public string SerializeData(T data)
        {
            return JsonUtility.ToJson(data);
        }
    }

    public class JSONBinarySerializer<T> : IDataSerializer<T, byte[]>
    {
        public T DeserializeData(byte[] data)
        {
            return JsonUtility.FromJson<T>(Encoding.ASCII.GetString(data));
        }

        public byte[] SerializeData(T data)
        {
            return Encoding.ASCII.GetBytes(JsonUtility.ToJson(data));
        }
    }
}