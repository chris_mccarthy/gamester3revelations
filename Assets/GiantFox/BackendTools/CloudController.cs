﻿using System;
using System.Collections.Generic;
using PlayerIOClient;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public class AssetAddress
    {
        public string tableId;
        public string key;
        public string property;

        public override string ToString()
        {
            return "AssetAddress, tableId: " + tableId + " key: " + key + " property: " + property;
        }
    }

    /// <summary>
    /// This serves as an interface with the Player.IO BigDB system
    /// It also caches these db objects for future use
    /// </summary>
    public class CloudController
    {
        private struct DBObjectKey
        {
            public string tableId;
            public string key;

            public override string ToString()
            {
                return "DBObjectKey, tableId: " + tableId + " key: " + key;
            }
        }

        [Inject] ClientState clientState;

        private Dictionary<DBObjectKey, DatabaseObject> cachedDatabaseObjects = new Dictionary<DBObjectKey, DatabaseObject>();
        private ReactiveProperty<List<DBObjectKey>> loadingDBObjects = new ReactiveProperty<List<DBObjectKey>>(new List<DBObjectKey>());

        public void SaveString(AssetAddress assetAddress, string data)
        {
            Save(assetAddress, obj => obj.Set(assetAddress.property, data));
        }

        public void SaveAsset(AssetAddress assetAddress, Byte[] data)
        {
            Save(assetAddress, obj => obj.Set(assetAddress.property, data));
        }

        private void Save(AssetAddress assetAddress, Action<DatabaseObject> applyToDatabaseObject)
        {
            var dbObjectKey = new DBObjectKey { tableId = assetAddress.tableId, key = assetAddress.key };

            Action<DatabaseObject> save = databaseObject =>
            {
                applyToDatabaseObject(databaseObject);
                cachedDatabaseObjects[dbObjectKey] = databaseObject;

                databaseObject.Save(
                    () => Debug.Log("Successfully uploaded " + assetAddress.key + " to table " + assetAddress.tableId),
                    error => Debug.LogError(error.Message));
            };

            loadingDBObjects
                .First(loadingDBObjects => !loadingDBObjects.Contains(dbObjectKey))
                .Subscribe(loadingDBObjects =>
                {
                    if (cachedDatabaseObjects.ContainsKey(dbObjectKey))
                    {
                        save(cachedDatabaseObjects[dbObjectKey]);
                    }
                    else
                    {
                        loadingDBObjects.Add(dbObjectKey);
                        this.loadingDBObjects.Value = loadingDBObjects;

                        clientState.cloudClient.Value.BigDB.LoadOrCreate(assetAddress.tableId, assetAddress.key, databaseObject =>
                        {
                            save(databaseObject);

                            loadingDBObjects.Remove(dbObjectKey);
                            this.loadingDBObjects.Value = loadingDBObjects;
                        },
                        error =>
                        {
                            Debug.LogError(error.Message);

                            loadingDBObjects.Remove(dbObjectKey);
                            this.loadingDBObjects.Value = loadingDBObjects;
                        }
                        );
                    }
                });
        }

        public IObservable<ICollection<string>> LoadProperties(AssetAddress assetAddress)
        {
            return Load(assetAddress)
                .Select(databaseObject => databaseObject.Properties);
        }

        public IObservable<byte[]> LoadAsset(AssetAddress assetAddress)
        {
            return Load(assetAddress)
                .Select(databaseObject => databaseObject.Contains(assetAddress.property) ? databaseObject.GetBytes(assetAddress.property) : null);
        }

        public IObservable<string> LoadString(AssetAddress assetAddress)
        {
            return Load(assetAddress)
                .Select(databaseObject => databaseObject.Contains(assetAddress.property) ? databaseObject.GetString(assetAddress.property) : null);
        }

        private IObservable<DatabaseObject> Load(AssetAddress assetAddress)
        {
            Subject<DatabaseObject> subject = new Subject<DatabaseObject>();

            // Wait a frame so you dont risk disposing instantly
            Observable.NextFrame()
                .Subscribe(_ =>
                {
                    var dbObjectKey = new DBObjectKey { tableId = assetAddress.tableId, key = assetAddress.key };

                    loadingDBObjects
                        .First(loadingDBObjects => !loadingDBObjects.Contains(dbObjectKey))
                        .Subscribe(loadingDBObjects =>
                        {
                            if (cachedDatabaseObjects.ContainsKey(dbObjectKey))
                            {
                                subject.OnNext(cachedDatabaseObjects[dbObjectKey]);
                                subject.Dispose();
                            }
                            else
                            {
                                loadingDBObjects.Add(dbObjectKey);
                                this.loadingDBObjects.Value = loadingDBObjects;

                                clientState.cloudClient.Value.BigDB.LoadOrCreate(assetAddress.tableId, assetAddress.key, databaseObject =>
                                {
                                    cachedDatabaseObjects[dbObjectKey] = databaseObject;
                                    subject.OnNext(databaseObject);
                                    subject.Dispose();

                                    loadingDBObjects.Remove(dbObjectKey);
                                    this.loadingDBObjects.Value = loadingDBObjects;
                                },
                                error =>
                                {
                                    Debug.LogError(error.Message);
                                    subject.Dispose();

                                    loadingDBObjects.Remove(dbObjectKey);
                                    this.loadingDBObjects.Value = loadingDBObjects;
                                });
                            }
                        });
                });

            return subject;
        }

        public void Delete(AssetAddress assetAddress)
        {
            var dbObjectKey = new DBObjectKey { tableId = assetAddress.tableId, key = assetAddress.key };

            loadingDBObjects
                .First(loadingDBObjects => !loadingDBObjects.Contains(dbObjectKey))
                .Subscribe(_ =>
                {
                    clientState.cloudClient.Value.BigDB.DeleteKeys(assetAddress.tableId, assetAddress.key);

                    cachedDatabaseObjects.Remove(dbObjectKey);
                });
        }
    }
}