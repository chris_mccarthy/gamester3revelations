﻿using System;
using System.Collections.Generic;
using System.Linq;
using GFS.Utilities;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public interface ICloudSaveStrategy<D>
    {
        IObservable<D> LoadData(CloudController cloudController, AssetAddress assetAddress);
        void SaveData(CloudController cloudController, AssetAddress assetAddress, D data);
    }

    public class SaveToCloudRepository<T, D> : IRepository<T>, IInitializable where T : IDependent
    {
        [Inject] private FriendsListController friendsListController;
        [Inject] private CloudController cloudController;
        [Inject] private ClientState clientState;
        [Inject] private ILockController lockController;
        [Inject] private DependencyValidator dependencyValidator;

        private Dictionary<string, ReactiveProperty<T>> data = new Dictionary<string, ReactiveProperty<T>>();
        private ReactiveProperty<List<string>> keys = new ReactiveProperty<List<string>>(new List<string>());

        private string tableId;
        private IDataSerializer<T, D> serializer;
        private ICloudSaveStrategy<D> saveStrategy;
        private bool subscribeToFriends;

        public IReadOnlyReactiveProperty<List<string>> Keys
        {
            get
            {
                return new CustomReadOnlyReactiveProperty<List<string>, List<string>>
                {
                    mutateValue = value => keys.Value.Where(key => dependencyValidator.ValidateData(data[key].Value)).ToList(),
                    property = keys
                };
            }
        }

        public IReadOnlyReactiveProperty<T> GetData(string key)
        {
            if (!data.ContainsKey(key))
            {
                Debug.LogError("Key not found in dictionary: " + key);
                return new ReactiveProperty<T>(default(T));
            }
            return data[key];
        }

        public IReadOnlyReactiveProperty<IDependent> GetDependent(string key)
        {
            return new CustomReadOnlyReactiveProperty<T, IDependent>
            {
                mutateValue = value => (IDependent)value,
                property = GetData(key)
            };
        }

        public SaveToCloudRepository(string tableId, IDataSerializer<T, D> serializer, ICloudSaveStrategy<D> saveStrategy, bool subscribeToFriends)
        {
            this.tableId = tableId;
            this.serializer = serializer;
            this.saveStrategy = saveStrategy;
            this.subscribeToFriends = subscribeToFriends;
        }

        public void Initialize()
        {
            if (subscribeToFriends)
                friendsListController.Friends
                    .Subscribe(LoadUserData);

            clientState.isCloudLoggedIn
                .DistinctUntilChanged()
                .Where(isCloudLoggedIn => isCloudLoggedIn)
                .Subscribe(_ =>
                {
                    LoadUserData(clientState.userId.Value);
                });

            clientState.isCloudLoggedIn
                .DistinctUntilChanged()
                .Where(isCloudLoggedIn => !isCloudLoggedIn)
                .Subscribe(_ =>
                {
                    data.Clear();
                    keys.Value = new List<string>();
                });
        }

        public void SetData(string key, T newData)
        {
            bool keyAdded = false;
            if (!data.ContainsKey(key))
            {
                data.Add(key, new ReactiveProperty<T>());
                keyAdded = true;
            }
            data[key].Value = newData;

            if (keyAdded)
                keys.Value = new List<string>(data.Keys);

            saveStrategy.SaveData(cloudController, new AssetAddress { tableId = tableId, key = clientState.userId.Value, property = key }, serializer.SerializeData(newData));
        }

        public void DeleteKey(string key)
        {
            if (data.Remove(key))
                keys.Value = new List<string>(data.Keys);

            cloudController.Delete(new AssetAddress { tableId = tableId, key = clientState.userId.Value, property = key });
        }

        private void LoadUserData(string userId)
        {
            clientState.isCloudLoggedIn
                .DistinctUntilChanged()
                .Where(isCloudLoggedIn => isCloudLoggedIn)
                .TakeUntil(clientState.isLoggedIn.Where(isloggedIn => !isloggedIn))
                .Subscribe(_ =>
                {
                    var propertiesLockToken = lockController.Lock();
                    // Load from cloud
                    cloudController.LoadProperties(new AssetAddress { tableId = tableId, key = userId })
                    .Subscribe(properties =>
                    {
                        lockController.Unlock(propertiesLockToken);

                        foreach (var key in properties)
                        {
                            var address = new AssetAddress { tableId = tableId, key = userId, property = key };
                            Debug.Log("Loading " + typeof(T) + ": " + address);

                            var lockToken = lockController.Lock();
                            saveStrategy.LoadData(cloudController, address)
                            .Select(data => serializer.DeserializeData(data))
                            .Subscribe(data =>
                            {
                                this.data.Add(key, new ReactiveProperty<T>(data));
                                var keyList = new List<string>(this.keys.Value);
                                keyList.Add(key);
                                this.keys.Value = keyList;

                                lockController.Unlock(lockToken);
                            });
                        }
                    });
                });
        }
    }

    public class SaveStringCloudStrategy : ICloudSaveStrategy<string>
    {
        public IObservable<string> LoadData(CloudController cloudController, AssetAddress assetAddress)
        {
            return cloudController.LoadString(assetAddress);
        }

        public void SaveData(CloudController cloudController, AssetAddress assetAddress, string data)
        {
            cloudController.SaveString(assetAddress, data);
        }
    }

    public class SaveBinaryCloudStrategy : ICloudSaveStrategy<byte[]>
    {
        public IObservable<byte[]> LoadData(CloudController cloudController, AssetAddress assetAddress)
        {
            return cloudController.LoadAsset(assetAddress);
        }

        public void SaveData(CloudController cloudController, AssetAddress assetAddress, byte[] data)
        {
            cloudController.SaveAsset(assetAddress, data);
        }
    }
}