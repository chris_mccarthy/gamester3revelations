﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public class TitleDataController : IInitializable
    {
        [Inject] ClientState clientState;

        public void Initialize()
        {
            clientState.isLoggedIn
                .Where(isLoggedIn => isLoggedIn)
                .Subscribe(_ =>
                {
                    PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(), results =>
                    {
                        clientState.titleData.Value = results.Data;
                    },
                    error => Debug.LogError(error.GenerateErrorReport()));
                });
        }
    }
}