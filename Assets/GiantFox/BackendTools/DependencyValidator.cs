﻿using System;
using System.Collections;
using System.Collections.Generic;
using GFS.BackendTools;
using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public interface IDependent
    {
        IEnumerable<IDependency> GetDependencies();
    }

    public interface IDependency
    {
        string GetId();
        Type GetRepositoryType();
    }

    public class Dependency : IDependency
    {
        public string id;
        public Type repositoryType;

        public string GetId()
        {
            return id;
        }

        public Type GetRepositoryType()
        {
            return repositoryType;
        }
    }

    public class DependencyValidator
    {
        [Inject] DiContainer container;

        public bool ValidateData(IDependent dependent)
        {
            foreach (var dependency in dependent.GetDependencies())
            {
                var id = dependency.GetId();
                var repo = container.Resolve(dependency.GetRepositoryType()) as IRepositoryBase;

                if (!repo.Keys.Value.Contains(id))
                {
                    Debug.LogError("Data of type " + dependent.GetType() + " is not valid because repository " + dependency.GetRepositoryType() + " does not contain key " + id);
                    return false;
                }

                var child = repo.GetDependent(id).Value;
                if (!ValidateData(child))
                    return false;
            }

            return true;
        }
    }
}