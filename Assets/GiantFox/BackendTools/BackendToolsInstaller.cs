﻿using UnityEngine;
using Zenject;

namespace GFS.BackendTools
{
    public class BackendToolsInstaller : MonoInstaller
    {
        [SerializeField] private string playerIOGameId;

        public override void InstallBindings()
        {
            //ClientState
            Container.BindInterfacesAndSelfTo<ClientState>()
                .AsSingle()
                .NonLazy();
            //LockController
            Container.BindInterfacesTo<LockController>()
                .AsSingle()
                .NonLazy();
            //CloudController
            Container.BindInterfacesAndSelfTo<CloudController>()
                .AsSingle()
                .NonLazy();
            //AuthenticationController
            Container.BindInterfacesAndSelfTo<AuthenticationController>()
                .AsSingle()
                .WithArguments(playerIOGameId)
                .NonLazy();
            //FriendsListController
            Container.BindInterfacesAndSelfTo<FriendsListController>()
                .AsSingle()
                .NonLazy();
            //TitleDataController
            Container.BindInterfacesAndSelfTo<TitleDataController>()
                .AsSingle()
                .NonLazy();
            Container.BindInterfacesAndSelfTo<DependencyValidator>()
                .AsSingle()
                .NonLazy();
        }
    }
}