﻿using System;
using System.Collections.Generic;
using System.IO;
using UniRx;
using UnityEngine;

namespace GFS.BackendTools
{
    public class SaveToDiskRepository<T> : IRepository<T>, IDisposable where T : IDependent
    {
        private Dictionary<string, ReactiveProperty<T>> data = new Dictionary<string, ReactiveProperty<T>>();
        private ReactiveProperty<List<string>> keys = new ReactiveProperty<List<string>>();

        private string directoryName;
        private CompositeDisposable disposables = new CompositeDisposable();
        private IDataSerializer<T, byte[]> serializer;

        public IReadOnlyReactiveProperty<List<string>> Keys
        {
            get
            {
                return keys;
            }
        }

        public IReadOnlyReactiveProperty<T> GetData(string key)
        {
            return data[key];
        }

        public SaveToDiskRepository(string fileName, IDataSerializer<T, byte[]> serializer)
        {
            this.directoryName = fileName;
            this.serializer = serializer;
            // Load data from disk
            var dataPath = Path.Combine(Application.persistentDataPath, fileName);
            if (Directory.Exists(dataPath))
            {
                foreach (var fileDirectory in Directory.GetFiles(dataPath))
                {
                    var dataBytes = File.ReadAllBytes(fileDirectory);
                    data.Add(Path.GetFileName(fileDirectory), new ReactiveProperty<T>(serializer.DeserializeData(dataBytes)));
                }
            }

            keys = new ReactiveProperty<List<string>>(new List<string>(data.Keys));
            disposables.Add(keys);
        }

        public void SetData(string key, T newData)
        {
            bool keyAdded = false;
            if (!data.ContainsKey(key))
            {
                data.Add(key, new ReactiveProperty<T>());
                keyAdded = true;
            }
            data[key].Value = newData;
            // Create the directory if it doesnt exist
            var directoryPath = Path.Combine(Application.persistentDataPath, directoryName);
            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            var dataBytes = serializer.SerializeData(newData);
            var dataPath = Path.Combine(directoryPath, key);
            // Save to disk
            File.WriteAllBytes(dataPath, dataBytes);

            if (keyAdded)
                keys.Value = new List<string>(data.Keys);
        }

        public void DeleteKey(string key)
        {
            if (data.Remove(key))
                keys.Value = new List<string>(data.Keys);

            var directoryPath = Path.Combine(Application.persistentDataPath, directoryName);
            if (Directory.Exists(directoryPath))
            {
                foreach (var filePath in Directory.GetFiles(directoryPath))
                {
                    File.Delete(filePath);
                }
                Directory.Delete(directoryPath);
            }
        }

        public void Dispose()
        {
            disposables.Dispose();
            foreach (var a in data.Values)
            {
                a.Dispose();
            }
            data.Clear();
        }

        public IReadOnlyReactiveProperty<IDependent> GetDependent(string key)
        {
            throw new NotImplementedException();
        }
    }
}