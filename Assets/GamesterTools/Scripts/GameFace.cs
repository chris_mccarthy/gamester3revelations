﻿using System.Linq;
using Gamester.Data;
using GFS.BackendTools;
using UnityEngine;
using Zenject;

namespace GamesterTools
{
    public class GameFace : MonoBehaviour
    {
        [SerializeField] string startingFaceId = "regular";
        [SerializeField] bool defaultToRegular = true;
        [SerializeField] TextureDisplays textureDisplays;

        [InjectOptional]
        private CharacterData characterData;
        [InjectOptional]
        private IRepository<CharacterFaceAsset> faceAssetRepository;

        private string currentFaceId;

        private void Start()
        {
            if (!string.IsNullOrEmpty(startingFaceId))
                ChangeFace(startingFaceId);
        }

        public void ChangeFace(string faceId)
        {
            if (faceAssetRepository != null && currentFaceId != faceId)
            {
                currentFaceId = faceId;

                var faceAsset = characterData.faceData.FirstOrDefault(face => face.id == faceId);

                if (faceAsset == null)
                {
                    if (defaultToRegular)
                        faceAsset = characterData.faceData.First();
                    else
                        textureDisplays.SetActive(false);
                }

                if (faceAsset != null)
                {
                    var faceAssetId = faceAsset.faceAssetId;
                    textureDisplays.SetActive(true);
                    var texture = faceAssetRepository.GetData(faceAssetId).Value.faceTexture;
                    texture.ApplyToDisplay(textureDisplays);
                }
            }
        }
    }
}