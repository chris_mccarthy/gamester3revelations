﻿using System.Linq;
using Gamester.Backend;
using Gamester.Data;
using Gamester.Game;
using GFS.BackendTools;
using UniRx;
using UnityEngine;
using Zenject;
using SimpleJSON;

namespace GamesterTools
{
    public class GameControls : MonoInstaller
    {
        [InjectOptional]
        GameState gameState;
        [InjectOptional]
        GenreCollection genreCollection;
        [InjectOptional]
        GameData gameData;
        [InjectOptional]
        IRepository<GameData> gameDataRepo;
        [InjectOptional]
        ClientState clientState;

        public override void InstallBindings()
        {
            Container.BindInstance(this);
        }

        public void EndGame()
        {
            if (gameState != null) gameState.endGame.OnNext(Unit.Default);
        }

        public void SetGameScore(string score)
        {
            if (gameState != null) gameState.gameScore.Value = score;
        }

        private GameCustomization GetGameCustomization(string category)
        {
            if (genreCollection == null)
                return null;
            var customizations = genreCollection.genres.First(genre => genre.genreID == gameData.genreId).customizations;
            var customizationId = gameData.customizations.First(data => data.category == category).id;
            var customizationAssets = customizations.FirstOrDefault(customization => customization.id == customizationId && customization.categoryId == category);
            return customizationAssets;
        }

        public GameObject InstantiateCustomization(string category)
        {
            var customizationAssets = GetGameCustomization(category);
            if (customizationAssets == null)
                return null;
            if (!(customizationAssets.asset is GameObject))
            {
                Debug.LogError("Asset of category " + category + " is not a GameObject");
                return null;
            }
            var prefab = customizationAssets.asset as GameObject;
            return Container.InstantiatePrefab(prefab);
        }

        private KeyedAsset GetKeyedAsset(string category, string key)
        {
            var customizationAssets = GetGameCustomization(category);
            if (customizationAssets == null)
                return null;
            if (!(customizationAssets.asset is CustomizationAssetCollection))
            {
                Debug.LogError("Asset of category " + category + " is not a CustomizationAssetCollection");
                return null;
            }
            var collection = customizationAssets.asset as CustomizationAssetCollection;
            var keyedAsset = collection.assets.FirstOrDefault(asset => asset.key == key);
            if (keyedAsset == null)
            {
                Debug.LogWarning("Asset of category " + category + " key " + key + " is null");
                return null;
            }
            return keyedAsset;
        }

        public GameObject InstantiateCustomization(string category, string key)
        {
            var keyedAsset = GetKeyedAsset(category, key);
            var prefab = keyedAsset.asset as GameObject;
            if (prefab == null)
            {
                Debug.LogError("Asset of category " + category + " key " + key + " is not a GameObject");
                return null;
            }
            return Container.InstantiatePrefab(prefab);
        }

        public Object GetCustomization(string category, string key)
        {
            var keyedAsset = GetKeyedAsset(category, key);
            return keyedAsset == null ? null : keyedAsset.asset;
        }

        public string GetGameTitleData()
        {
            if (gameDataRepo == null)
                return string.Empty;
            return clientState.titleData.Value[gameData.genreId];
        }

        public string GetGameProgression()
        {
            var jsonString = GetGameTitleData();
            if (string.IsNullOrEmpty(jsonString))
                return string.Empty;

            var json = JSON.Parse(jsonString);
            return json.Linq.First(kvp => kvp.Key == "progression").Value.ToString();
        }

        public string GetSetting(string property)
        {
            var jsonString = GetGameTitleData();
            if (string.IsNullOrEmpty(jsonString))
                return string.Empty;

            var json = JSON.Parse(jsonString);
            var settingsJson = json.Linq.First(kvp => kvp.Key == "settings").Value;
            return settingsJson[property].Value;
        }

        public void PauseGame()
        {
            gameState.isPaused.Value = true;
        }
    }
}