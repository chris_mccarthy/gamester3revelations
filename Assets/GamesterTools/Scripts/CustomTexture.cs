﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace GamesterTools
{
    public class CustomTexture : MonoBehaviour
    {
        [SerializeField] private string category;
        [SerializeField] private string key;

        [SerializeField] private TextureDisplays textureDisplays;

        [Inject] private GameControls gameControls;

        private void Start()
        {
            if (gameControls == null)
            {
                Debug.LogWarning("GameControls not found, deactivating");
                return;
            }
            var texture = gameControls.GetCustomization(category, key) as Texture2D;

            if (texture == null)
                textureDisplays.SetActive(false);
            else
                texture.ApplyToDisplay(textureDisplays);

        }
    }
}