﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamesterTools
{
    [RequireComponent(typeof(Canvas))]
    public class AutoConfigureWorldCanvas : MonoBehaviour
    {
        [SerializeField] string cameraTag;
        private void Start()
        {
            var cameraGameObject = GameObject.FindWithTag(cameraTag);
            if (cameraGameObject != null)
            {
                var camera = cameraGameObject.GetComponent<Camera>();
                var canvas = GetComponent<Canvas>();
                canvas.renderMode = RenderMode.ScreenSpaceCamera;
                canvas.worldCamera = camera;
            }
        }
    }
}