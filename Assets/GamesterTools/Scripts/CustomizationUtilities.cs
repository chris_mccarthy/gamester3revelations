﻿using Anima2D;
using Gamester.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace GamesterTools
{
    [System.Serializable]
    public struct TextureDisplays
    {
        public RawImage faceImage;
        public AspectRatioFitter fitter;
        public SpriteRenderer spriteRenderer;
        public SpriteMeshInstance spriteMeshInstance;

        public void SetActive(bool active)
        {
            if (faceImage != null)
                faceImage.gameObject.SetActive(active);

            if (spriteRenderer != null)
                spriteRenderer.gameObject.SetActive(active);

            if (spriteMeshInstance != null)
                spriteMeshInstance.gameObject.SetActive(active);
        }
    }

    public static class CustomizationUtilities
    {
        public static void ApplyToDisplay(this Texture2D texture, TextureDisplays textureDisplays)
        {
            if (textureDisplays.faceImage != null)
                textureDisplays.faceImage.texture = texture;

            if (textureDisplays.fitter != null)
                textureDisplays.fitter.aspectRatio = ((float)texture.width) / ((float)texture.height);

            if (textureDisplays.spriteRenderer != null)
                textureDisplays.spriteRenderer.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * .5f);

            if (textureDisplays.spriteMeshInstance != null)
            {
                var scaledTexture = new Texture2D(4, 4);
                var appliedTexture = textureDisplays.spriteMeshInstance.spriteMesh.sprite.texture;
                texture.Scale(scaledTexture, appliedTexture.width, appliedTexture.height, true);
                appliedTexture.SetPixels(scaledTexture.GetPixels());
                appliedTexture.Apply();
            }
        }
    }
}