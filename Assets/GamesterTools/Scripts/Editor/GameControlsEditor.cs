﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GamesterTools
{
    [CustomEditor(typeof(GameControls))]
    public class GameControlsEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Trigger Continue Event"))
                PlayMakerFSM.BroadcastEvent("Continue");

            base.OnInspectorGUI();
        }
    }
}