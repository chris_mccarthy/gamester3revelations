﻿using System.Collections;
using System.Collections.Generic;
using HutongGames.PlayMaker;
using UnityEngine;
using SimpleJSON;
using TooltipAttribute = HutongGames.PlayMaker.TooltipAttribute;
using System.Linq;

namespace GamesterTools.FSM
{
    [Tooltip("Parse a json array into an array")]
    public class GetJSONArray : FsmStateAction
    {
        [RequiredField]
        [Tooltip("The string of the serialized json")]
        public FsmString jsonString;

        [RequiredField]
        [Tooltip("The name of the property within the json array elements that is being stored in the array")]
        public FsmString propertyName;

        [RequiredField]
        [Tooltip("Stores the value to the array")]
        [UIHint(UIHint.Variable)]
        public FsmArray storeValue;

        public override void Reset()
        {
            propertyName = null;
            storeValue = null;
        }

        public override void OnEnter()
        {
            var jsonStringValue = jsonString.Value;
            if (!string.IsNullOrEmpty(jsonStringValue))
            {
                var jsonObject = JSON.Parse(jsonStringValue);
                var count = jsonObject.Count;

                storeValue.Resize(count);

                for (int a = 0; a < count; a++)
                {
                    var child = jsonObject[a].Linq.First(c => c.Key == propertyName.Value).Value;

                    if (child.IsNumber)
                    {
                        storeValue.Set(a, child.AsInt);
                    }
                    else if (child.IsString)
                    {
                        storeValue.Set(a, child.Value);
                    }
                    else if (child.IsBoolean)
                    {
                        storeValue.Set(a, child.AsBool);
                    }
                }
            }
            Finish();
        }
    }
}